<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\TemplateDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TemplateDetailController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
          'mst_vlan_template_detail.HeaderCode AS HeaderCode',
          'mst_vlan_template_detail.Vlan AS Vlan',
          'mst_vlan_template_detail.UserVlan AS UserVlan',
          'mst_vlan_template_detail.InnerVlan AS InnerVlan',
          'mst_vlan_template_detail.TagTransform AS TagTransform',
          'mst_vlan_template_detail.TrafficTable AS TrafficTable',
          'mst_vlan_template_detail.GemPort AS GemPort',
          'mst_vlan_template_detail.Remark AS Remark'
       ]);

       if ($request->HeaderCode) {
           $query->where('HeaderCode', '=', $request->HeaderCode);
       }
       // dd($query);
       // $query->where('HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }


    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
            'mst_vlan_template_detail.HeaderCode AS HeaderCode',
            'mst_vlan_template_detail.Vlan AS Vlan',
            'mst_vlan_template_detail.UserVlan AS UserVlan',
            'mst_vlan_template_detail.InnerVlan AS InnerVlan',
            'mst_vlan_template_detail.TagTransform AS TagTransform',
            'mst_vlan_template_detail.TrafficTable AS TrafficTable',
            'mst_vlan_template_detail.GemPort AS GemPort',
            'mst_vlan_template_detail.Remark AS Remark'
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'HeaderCode' => 'nullable|max:250',
            'Vlan' => 'nullable|max:250',
            'UserVlan' => 'nullable|max:250',
            'InnerVlan' => 'nullable|max:250',
            'TagTransform' => 'nullable|max:250',
            'TrafficTable' => 'nullable|max:250',
            'GemPort' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new TemplateDetail();
            // dd($request);
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }
        if ($request->HeaderCode) {
            $data->HeaderCode = $request->HeaderCode;
        }
        if ($request->Vlan or $request->Vlan == 0) {
            $data->Vlan = $request->Vlan;
        }
        if ($request->UserVlan or $request->UserVlan == 0) {
            $data->UserVlan = $request->UserVlan;
        }
        if ($request->InnerVlan or $request->InnerVlan == 0) {
            $data->InnerVlan = $request->InnerVlan;
        }
        if ($request->TagTransform or $request->TagTransform == 0) {
            $data->TagTransform = $request->TagTransform;
        }
        if ($request->TrafficTable or $request->TrafficTable == 0) {
            $data->TrafficTable = $request->TrafficTable;
        }
        if ($request->GemPort or $request->GemPort == 0) {
            $data->GemPort = $request->GemPort;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->InActiveBy) {
            $data->InActiveBy = $request->InActiveBy;
        }
        if ($request->InActiveDate) {
            $data->InActiveDate = $request->InActiveDate;
        }
        // if ($request->ActiveStatus) {
        //     $data->ActiveStatus = to_bool($request->ActiveStatus);
        // }
        $data->save();

        return $data;
    }

}
