<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\CustomerSwitch;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerSwitchController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'LinkID'=> 'nullable|max:20',
            'MemberID'=> 'nullable|max:20',
            'Name'=> 'nullable|max:20',
            'JobTitle'=> 'nullable|max:20',
            'Email'=> 'nullable|max:20',
            'Phone1'=> 'nullable|max:20',
            'Phone2'=> 'nullable|max:20',
            'Fax'=> 'nullable|max:20',
            'NPWP'=> 'nullable|max:20',
            'Address'=> 'nullable|max:20',
            'City'=> 'nullable|max:20',
            'CityCode'=> 'nullable|max:20',
            'Province'=> 'nullable|max:20',
            'ZipCode'=> 'nullable|max:20',
            'Website'=> 'nullable|max:20',
            'IDCard'=> 'nullable|max:20',
            'IDCardNumber'=> 'nullable|max:20',
            'IDCardExpired'=> 'nullable|max:20',
            'RegistrationDate'=> 'nullable|max:20',
            'ProductCode'=> 'nullable|max:20',
            'ProductGrup'=> 'nullable|max:20',
            'ProductPort'=> 'nullable|max:20',
            'ProductName'=> 'nullable|max:20',
            'ProductNotes'=> 'nullable|max:20',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new CustomerSwitch();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->Name) {
            $data->Name = $request->Name;
        }
        if ($request->CompanyName) {
            $data->CompanyName = $request->CompanyName;
        }
        if ($request->LinkID) {
            $data->LinkID = $request->LinkID;
        }
        if ($request->MemberID) {
            $data->MemberID = $request->MemberID;
        }
        if ($request->JobTitle) {
            $data->JobTitle = $request->JobTitle;
        }
        if ($request->ContactPerson) {
            $data->ContactPerson = $request->ContactPerson;
        }
        if ($request->Email) {
            $data->Email = $request->Email;
        }
        if ($request->Phone1) {
            $data->Phone1 = $request->Phone1;
        }
        if ($request->Phone2) {
            $data->Phone2 = $request->Phone2;
        }
        if ($request->PhoneNumber) {
            $data->PhoneNumber = $request->PhoneNumber;
        }
        if ($request->NPWP) {
            $data->NPWP = $request->NPWP;
        }
        if ($request->Fax) {
            $data->Fax = $request->Fax;
        }
        if ($request->Address) {
            $data->Address = $request->Address;
        }
        if ($request->CityCode) {
            $data->CityCode = $request->CityCode;
        }
        if ($request->City) {
            $data->City = $request->City;
        }
        if ($request->Province) {
            $data->Province = $request->Province;
        }
        if ($request->ZipCode) {
            $data->ZipCode = $request->ZipCode;
        }
        if ($request->Website) {
            $data->Website = $request->Website;
        }
        if ($request->IDCard) {
            $data->IDCard = $request->IDCard;
        }
        if ($request->IDCardNumber) {
            $data->IDCardNumber = $request->IDCardNumber;
        }
        if ($request->IDCardExpired) {
            $data->IDCardExpired = $request->IDCardExpired;
        }
        if ($request->RegistrationDate) {
            $data->RegistrationDate = $request->RegistrationDate;
        }
        if ($request->ProductCode) {
            $data->ProductCode = $request->ProductCode;
        }
        if ($request->ClientStatus) {
            $data->ClientStatus = $request->ClientStatus;
        }
        if ($request->ProductSite) {
            $data->ProductSite = $request->ProductSite;
        }
        if ($request->ProductGrup) {
            $data->ProductGrup = $request->ProductGrup;
        }
        if ($request->ProductPort) {
            $data->ProductPort = $request->ProductPort;
        }
        if ($request->ProductCategory) {
            $data->ProductCategory = $request->ProductCategory;
        }
        if ($request->ProductName) {
            $data->ProductName = $request->ProductName;
        }
        if ($request->ProductNotes) {
            $data->ProductNotes = $request->ProductNotes;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = $request->ActiveStatus;
        }
        $data->save();

        return $data;
    }

}
