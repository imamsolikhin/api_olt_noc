<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\OntInfo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OntInfoController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       $query->where('HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new OntInfo();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->CustomerCode) {
            $data->CustomerCode = $request->CustomerCode;
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->FrameId) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotID) {
            $data->SlotID = $request->SlotID;
        }
        if ($request->PortId) {
            $data->PortId = $request->PortId;
        }
        if ($request->OntId) {
            $data->OntId = $request->OntId;
        }
        if ($request->OntSn) {
            $data->OntSn = $request->OntSn;
        }
        if ($request->Flag) {
            $data->Flag = $request->Flag;
        }
        if ($request->Run) {
            $data->Run = $request->Run;
        }
        if ($request->Config) {
            $data->Config = $request->Config;
        }
        if ($request->Match) {
            $data->Match = $request->Match;
        }
        if ($request->OntDistance) {
            $data->OntDistance = $request->OntDistance;
        }
        if ($request->Temperature) {
            $data->Temperature = $request->Temperature;
        }
        if ($request->Description) {
            $data->Description = $request->Description;
        }
        if ($request->LastDownCause) {
            $data->LastDownCause = $request->LastDownCause;
        }
        if ($request->LastUpTime) {
            $data->LastUpTime = $request->LastUpTime;
        }
        if ($request->LastDownTime) {
            $data->LastDownTime = $request->LastDownTime;
        }
        if ($request->LastDyingGaspTime) {
            $data->LastDyingGaspTime = $request->LastDyingGaspTime;
        }
        if ($request->OntOnlineDuration) {
            $data->OntOnlineDuration = $request->OntOnlineDuration;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
