<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerSiteController extends Controller
{

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        $query->select('*');
        if ($request->ActiveStatus) {
            $query->where('ActiveStatus', '=', $request->ActiveStatus);
        }
        // $query->where('LinkId', '!=', Null);
        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::select([
            '*'
        ])->find(str_replace('%20', ' ', $id));
        return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'LinkID' => 'nullable|max:250',
            'IDPelanggan' => 'nullable|max:250',
            'CompanyName' => 'nullable|max:250',
            'Province' => 'nullable|max:250',
            'City' => 'nullable|max:250',
            'Address' => 'nullable|max:250',
            'ZipCode' => 'nullable|max:250',
            'Website' => 'nullable|max:250',
            'Email' => 'nullable|max:250',
            'Phone' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'ProductSite' => 'nullable|max:250',
            'ProductGrup' => 'nullable|max:250',
            'ProductPort' => 'nullable|max:250',
            'ProductCategory' => 'nullable|max:250',
            'ProductName' => 'nullable|max:250',
            'ProductNotes' => 'nullable|max:250',
            'ActiveStatus'  => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new CustomerSite();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->LinkID) {
            $data->LinkID = $request->LinkID;
        }
        if ($request->IDPelanggan) {
            $data->IDPelanggan = $request->IDPelanggan;
        }
        if ($request->CompanyName) {
            $data->CompanyName = $request->CompanyName;
        }
        if ($request->Province) {
            $data->Province = $request->Province;
        }
        if ($request->City) {
            $data->City = $request->City;
        }
        if ($request->Address) {
            $data->Address = $request->Address;
        }
        if ($request->ZipCode) {
            $data->ZipCode = $request->ZipCode;
        }
        if ($request->Website) {
            $data->Website = $request->Website;
        }
        if ($request->Email) {
            $data->Email = $request->Email;
        }
        if ($request->Phone) {
            $data->Phone = $request->Phone;
        }
        if ($request->Fax) {
            $data->Fax = $request->Fax;
        }
        if ($request->ProductSite) {
            $data->ProductSite = $request->ProductSite;
        }
        if ($request->ProductGrup) {
            $data->ProductGrup = $request->ProductGrup;
        }
        if ($request->ProductPort) {
            $data->ProductPort = $request->ProductPort;
        }
        if ($request->ProductCategory) {
            $data->ProductCategory = $request->ProductCategory;
        }
        if ($request->ProductName) {
            $data->ProductName = $request->ProductName;
        }
        if ($request->ProductNotes) {
            $data->ProductNotes = $request->ProductNotes;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }
}
