<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\Host;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ScriptController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Hostname' => 'nullable|max:250',
            'IpAddress' => 'nullable|max:250',
            'Username' => 'nullable|max:250',
            'Password' => 'nullable|max:250',
            'Port' => 'nullable|max:250',
            'SnmpCommunity' => 'nullable|max:250',
            'Sysname' => 'nullable|max:250',
            'DeviceModel' => 'nullable|max:250',
            'DeviceVersion' => 'nullable|max:250',
            'Remark' => 'nullable',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Host();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->Hostname) {
            $data->Hostname = $request->Hostname;
        }
        if ($request->IpAddress) {
            $data->IpAddress = $request->IpAddress;
        }
        if ($request->User) {
            $data->Username = $request->User;
        }
        if ($request->Pass) {
            $data->Password = $request->Pass;
        }
        if ($request->Port) {
            $data->Port = $request->Port;
        }
        if ($request->Sysname) {
            $data->Sysname = $request->Sysname;
        }
        if ($request->SnmpCommunity) {
            $data->SnmpCommunity = $request->SnmpCommunity;
        }
        if ($request->DeviceModel) {
            $data->DeviceModel = $request->DeviceModel;
        }
        if ($request->DeviceVersion) {
            $data->DeviceVersion = $request->DeviceVersion;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->InActiveBy) {
            $data->InActiveBy = $request->InActiveBy;
        }
        if ($request->InActiveDate) {
            $data->InActiveDate = $request->InActiveDate;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = $request->ActiveStatus;
        }
        $data->save();

        return $data;
    }

}
