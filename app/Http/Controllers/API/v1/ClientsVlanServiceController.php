<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\ClientsVlanService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClientsVlanServiceController extends Controller
{

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        if ($request->ActiveStatus) {
            $query->where('ActiveStatus', '=', $request->ActiveStatus);
        }
        if ($request->CustomerCode) {
            $query->where('CustomerCode', '=', $request->CustomerCode);
        }
        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));
        return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'CustomerCode' => 'nullable|max:250',
            'ServicePortId' => 'nullable|max:250',
            'GemPort' => 'nullable|max:250',
            'UserVlan' => 'nullable|max:250',
            'InnerVlan' => 'nullable|max:250',
            'TagTransform' => 'nullable|max:250',
            'TrafficTable' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new ClientsVlanService();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->Vlan or $request->Vlan == 0) {
            $data->Vlan = $request->Vlan;
        }
        if ($request->ServicePortId or $request->ServicePortId == 0) {
            $data->ServicePortId = $request->ServicePortId;
        }
        if ($request->GemPort or $request->GemPort == 0) {
            $data->GemPort = $request->GemPort;
        }
        if ($request->CustomerCode or $request->CustomerCode == 0) {
            $data->CustomerCode = $request->CustomerCode;
        }
        if ($request->UserVlan or $request->UserVlan == 0) {
            $data->UserVlan = $request->UserVlan;
        }
        if ($request->InnerVlan or $request->InnerVlan == 0) {
            $data->InnerVlan = $request->InnerVlan;
        }
        if ($request->TagTransform) {
            $data->TagTransform = $request->TagTransform;
        }
        if ($request->TrafficTable or $request->TrafficTable == 0) {
            $data->TrafficTable = $request->TrafficTable;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }
}
