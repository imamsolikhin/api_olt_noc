<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\OntLast;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class OntLastController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
                   'olt_ont_last.Code AS Code',
                   'olt_ont_last.HostCode AS HostCode',
                   'olt_ont_last.Sn AS Sn',
                   'olt_ont_last.Version AS Version',
                   'olt_ont_last.SoftwareVersion AS SoftwareVersion',
                   'olt_ont_last.EquipmentId AS EquipmentId',
                   'olt_ont_last.FrameId AS FrameId',
                   'olt_ont_last.SlotId AS SlotId',
                   'olt_ont_last.PortId AS PortId',
                   'olt_ont_last.Remark AS Remark',
                   'olt_ont_last.CreatedBy AS CreatedBy',
                   'olt_ont_last.CreatedDate AS CreatedDate',
                   'olt_host.FrameId AS BoardFrameId',
                   'olt_host.SlotId AS BoardSlotId',
                   'olt_host.PortId AS BoardPortId',
                   'olt_host.Hostname AS Hostname',
                   'olt_host.IpAddress AS IpAddress',
                   'olt_host.Port AS Port',
                   'olt_host.Sysname AS Sysname',
                   'olt_host.SnmpCommunity AS SnmpCommunity',
       ]);
       $query->leftjoin('olt_host', 'olt_host.code', '=', 'olt_ont_last.HostCode');
       if($request->ActiveStatus){
           $query->where('olt_ont_last.ActiveStatus', '=', $request->ActiveStatus);
       }
       if($request->HostCode){
            $query->where('HostCode', '=', $request->HostCode);
        }
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
               'olt_ont_last.Code AS Code',
               'olt_ont_last.HostCode AS HostCode',
               'olt_ont_last.Sn AS Sn',
               'olt_ont_last.Version AS Version',
               'olt_ont_last.SoftwareVersion AS SoftwareVersion',
               'olt_ont_last.EquipmentId AS EquipmentId',
               'olt_ont_last.FrameId AS FrameId',
               'olt_ont_last.SlotId AS SlotId',
               'olt_ont_last.PortId AS PortId',
               'olt_ont_last.Remark AS Remark',
               'olt_ont_last.CreatedBy AS CreatedBy',
               'olt_ont_last.CreatedDate AS CreatedDate',
               'olt_host.FrameId AS BoardFrameId',
               'olt_host.SlotId AS BoardSlotId',
               'olt_host.PortId AS BoardPortId',
               'olt_host.Hostname AS Hostname',
               'olt_host.IpAddress AS IpAddress',
               'olt_host.Port AS Port',
               'olt_host.Username AS Username',
               'olt_host.Password AS Password',
               'olt_host.Sysname AS Sysname',
               'olt_host.SnmpCommunity AS SnmpCommunity',
             ])
          ->leftjoin('olt_host', 'olt_host.code', '=', 'olt_ont.HostCode')
          ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'HostCode' => 'nullable|max:250',
            'Sn' => 'nullable|max:250',
            'Version' => 'nullable|max:250',
            'SoftwareVersion' => 'nullable|max:250',
            'EquipmentId' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'RxPower' => 'nullable|max:250',
            'TxPower' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new OntLast();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->HostCode) {
          $data->HostCode = $request->HostCode;
        }
        if ($request->Sn) {
          $data->Sn = $request->Sn;
        }
        if ($request->Version) {
          $data->Version = $request->Version;
        }
        if ($request->SoftwareVersion) {
          $data->SoftwareVersion = $request->SoftwareVersion;
        }
        if ($request->EquipmentId) {
          $data->EquipmentId = $request->EquipmentId;
        }
        if ($request->FrameId) {
          $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId) {
          $data->SlotId = $request->SlotId;
        }
        if ($request->PortId) {
          $data->PortId = $request->PortId;
        }
        if ($request->RxPower) {
          $data->RxPower = $request->RxPower;
        }
        if ($request->TxPower) {
          $data->TxPower = $request->TxPower;
        }
        if ($request->Remark) {
          $data->Remark = $request->Remark;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
