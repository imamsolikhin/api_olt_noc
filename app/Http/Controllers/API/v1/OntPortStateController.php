<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\OntPortState;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OntPortStateController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       $query->where('HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
      $data = getModelName($table)::select('*')
              ->where('CustomerCode', $id)
              ->get();
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new OntPortState();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->CustomerCode) {
            $data->CustomerCode = $request->CustomerCode;
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->FrameId) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId) {
            $data->SlotId = $request->SlotId;
        }
        if ($request->PortId) {
            $data->PortId = $request->PortId;
        }
        if ($request->OntId) {
            $data->OntId = $request->OntId;
        }
        if ($request->OntSn) {
            $data->OntSn = $request->OntSn;
        }
        if ($request->OntPortID) {
            $data->OntPortID = $request->OntPortID;
        }
        if ($request->OntPortType) {
            $data->OntPortType = $request->OntPortType;
        }
        if ($request->Speed) {
            $data->Speed = $request->Speed;
        }
        if ($request->Duplex) {
            $data->Duplex = $request->Duplex;
        }
        if ($request->LinkState) {
            $data->LinkState = $request->LinkState;
        }
        if ($request->RingStatus) {
            $data->RingStatus = $request->RingStatus;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
