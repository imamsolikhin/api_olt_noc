<?php
namespace App\Http\Controllers\API\v1;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Datatables;

class OltController extends Controller
{
    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Display a listing of the resource.
     *
     * @param string $table
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function dashboard($table, Request $request)
    {
        $data = getControllerName($table)::dashboard($request,$table);
        return makeResponse(200, 'success', 'Dashboard ' . str_replace('-', ' ', $table) . ' has been loaded successfully', $data);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Display a listing of the resource.
     *
     * @param string $table
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function datatable($table, Request $request)
    {
        $data = Datatables::of(\DB::table('mst_customer_noc')->take(10))->make(true);
        // dd($data);
        return $data;
    }


    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Display a listing of the resource.
     *
     * @param string $table
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function index($table, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required_with:page|integer|min:1',
            'page' => 'required_with:limit|integer|min:1',
            'search' => 'nullable'
        ]);
        if ($validator->fails())
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getModelName($table)::when($request, function ($query, $request) use ($table) {
            $query = getControllerName($table)::searchQuery($query, $request);
            return $query;
      })->paginate($request->limit,['*'],'page',$request->page);
        return makeResponse(200, 'pagination', null, getResourceName($table)::collection($data));
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Store a newly created resource in database.
     *
     * @param string $table
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function store($table, Request $request)
    {
        $validator = getControllerName($table)::validation($request);

        if ($validator->fails())
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $resource = getResourceName($table);
        $data = null;
        if($request->list){
          $list = $request->data;
          for ($i=0; $i < count($list); $i++) {
            $object = json_decode(json_encode($list[$i]));
            $data = getControllerName($table)::save($object,null,$table);
            // return $data;
          }
        }else{
          $data = getControllerName($table)::save($request,null,$table);
          Log::info('[' . $data->getTable() . '.create] ' . json_encode(array_merge([
              'ClientIP' => $request->ip()
          ], [
              'Data' => new $resource($data)
          ])));
        }

        return makeResponse(200, 'success', 'new ' . str_replace('-', ' ', $table) . ' has been save successfully', new $resource($data));
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Display the specified resource.
     *
     * @param string $table
     * @param string $id
     * @return json
     */
    public function show($table, $id, Request $request)
    {
      if($request->setting){
        $data = getControllerName($table)::setting($table, $id, $request);
          return makeResponse(200, 'success', null, $data);
      }
        $data = getControllerName($table)::searchId($table, $id, $request);

        if (! $data)
            return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);
        if($table == "ont-port-state"){
          return makeResponse(200, 'success', null, getResourceName($table)::collection($data));
        }
        return makeResponse(200, 'success', null, new $resource($data));
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Update the specified resource in database.
     *
     * @param string $table
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return json
     */
    public function update($table, Request $request, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));
        if (! $data)
            return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $validator = getControllerName($table)::validation($request, 'update');

        if ($validator->fails())
            return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.edit] ' . json_encode(array_merge([
            'ClientIP' => $request->ip()
        ], [
            'OldData' => new $resource($data),
            'NewData' => array_merge([
                $data->getKeyName() => str_replace('%20', ' ', $id)
            ], $request->all())
        ])));
        $data = getControllerName($table)::save($request, $data, $table);

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been update successfully', new $resource($data));
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Remove the specified resource from database.
     *
     * @param string $table
     * @param string $id
     * @return json
     */
    public function destroy($table, $id, Request $request)
    {
        if (isset($request->Config)){
          $data = getModelName($table)::where('Code', '!=', $request->Config)->delete();
        }elseif(isset($request->CustomerCode)){
          $data = getModelName($table)::where('CustomerCode', '=', $request->CustomerCode)->delete();
        }elseif(isset($request->HeaderCode)){
          $data = getModelName($table)::where('HeaderCode', '=', $request->HeaderCode)->delete();
        }elseif(isset($request->HostCode)){
          $data = getModelName($table)::where('HostCode', '=', $request->HostCode)->delete();
        }else{
          $data = getModelName($table)::find(str_replace('%20', ' ', $id));

          if (! $data)
              return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

          $resource = getResourceName($table);

          Log::info('[' . $data->getTable() . '.delete] ' . json_encode(array_merge([
              'ClientIP' => request()->ip()
          ], [
              'Data' => new $resource($data)
          ])));
          $data->delete();
        }

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been delete successfully');
    }
}
