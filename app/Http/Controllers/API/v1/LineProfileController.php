<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\LineProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LineProfileController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select(\DB::raw('olt_line_profile.*'));
       // $query->select(\DB::raw('count(olt_gem_profile.code) as data'));
       // $query->leftjoin('olt_gem_profile', 'olt_gem_profile.ProfileId', '=', 'olt_line_profile.ProfileId');
       if($request->ActiveStatus){
           $query->where('olt_line_profile.ActiveStatus', '=', $request->ActiveStatus);
       }
       // if($request->HostCode){
       //     $query->where('olt_line_profile.HostCode', '=', $request->HostCode);
       // }
       // $query->groupBy('olt_gem_profile.ProfileId');
       $query->where('olt_line_profile.HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'ProfileId' => 'nullable|max:250',
            'ProfileName' => 'nullable|max:250',
            'ProfileBinding' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new LineProfile();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->ProfileId) {
            $data->ProfileId = $request->ProfileId;
        }
        if ($request->ProfileName) {
            $data->ProfileName = $request->ProfileName;
        }
        if ($request->ProfileBinding) {
            $data->ProfileBinding = $request->ProfileBinding;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
