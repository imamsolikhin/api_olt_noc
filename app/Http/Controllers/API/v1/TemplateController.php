<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\Template;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select([
             'mst_vlan_template.Code AS Code',
             'mst_vlan_template.Name AS Name',
             'mst_vlan_template.VlanDownLink AS VlanDownLink',
             'mst_vlan_template.OntLineProfileId AS OntLineProfileId',
             'mst_vlan_template.OntSrvProfileId AS OntSrvProfileId',
             'mst_vlan_template.NativeVlanEth1 AS NativeVlanEth1',
             'mst_vlan_template.NativeVlanEth2 AS NativeVlanEth2',
             'mst_vlan_template.NativeVlanEth3 AS NativeVlanEth3',
             'mst_vlan_template.NativeVlanEth4 AS NativeVlanEth4',
             'mst_vlan_template.VlanAttribut AS VlanAttribut',
             'mst_vlan_template.Remark AS Remark',
       ]);

       if($request->from_date != '' && $request->from_date  != ''){
         $query->whereBetween('mst_vlan_template.CreatedDate', array($request->from_date, $request->to_date)) ;
       }

       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
            'mst_vlan_template.Code AS Code',
            'mst_vlan_template.Name AS Name',
            'mst_vlan_template.VlanDownLink AS VlanDownLink',
            'mst_vlan_template.OntLineProfileId AS OntLineProfileId',
            'mst_vlan_template.OntSrvProfileId AS OntSrvProfileId',
            'mst_vlan_template.NativeVlanEth1 AS NativeVlanEth1',
            'mst_vlan_template.NativeVlanEth2 AS NativeVlanEth2',
            'mst_vlan_template.NativeVlanEth3 AS NativeVlanEth3',
            'mst_vlan_template.NativeVlanEth4 AS NativeVlanEth4',
            'mst_vlan_template.VlanAttribut AS VlanAttribut',
            'mst_vlan_template.Remark AS Remark',
                  ])
            ->find(str_replace('%20', ' ', $id));
              return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Name' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'VlanAttribut' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Template;
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->except('Name')) {
            $data->Name = $request->Name;
        }
        if ($request->except('VlanDownLink') or $request->except('VlanDownLink') == 0) {
            $data->VlanDownLink = $request->VlanDownLink;
        }
        if ($request->except('OntLineProfileId') or $request->except('OntLineProfileId') == 0) {
            $data->OntLineProfileId = $request->OntLineProfileId;
        }
        if ($request->except('OntSrvProfileId') or $request->except('OntSrvProfileId') == 0) {
            $data->OntSrvProfileId = $request->OntSrvProfileId;
        }
        if ($request->except('NativeVlanEth1') or $request->except('NativeVlanEth1') == 0) {
            $data->NativeVlanEth1 = $request->NativeVlanEth1;
        }
        if ($request->except('NativeVlanEth2') or $request->except('NativeVlanEth2') == 0) {
            $data->NativeVlanEth2 = $request->NativeVlanEth2;
        }
        if ($request->except('NativeVlanEth3') or $request->except('NativeVlanEth3') == 0) {
            $data->NativeVlanEth3 = $request->NativeVlanEth3;
        }
        if ($request->except('NativeVlanEth4') or $request->except('NativeVlanEth4') == 0) {
            $data->NativeVlanEth4 = $request->NativeVlanEth4;
        }
        if ($request->except('VlanAttribut')) {
            $data->VlanAttribut = $request->VlanAttribut;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        $data->save();

        return $data;
    }

}
