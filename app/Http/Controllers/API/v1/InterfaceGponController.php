<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\InterfaceGpon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class InterfaceGponController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('olt_interface_gpon_ont.*','mst_customer_erp.CompanyName as Description');
       $query->leftjoin('mst_customer_noc',\DB::raw('CONCAT(mst_customer_noc.OntId,"-",mst_customer_noc.OntSn)'), '=', \DB::raw('CONCAT(olt_interface_gpon_ont.OntId,"-",olt_interface_gpon_ont.OntSn)'));
       $query->leftjoin('mst_customer_erp', 'mst_customer_erp.Code', '=', 'mst_customer_noc.CustomerCode');

       if($request->ActiveStatus){
           $query->where('olt_interface_gpon_ont.ActiveStatus', '=', $request->ActiveStatus);
       }
       $query->where('olt_interface_gpon_ont.HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       $query->groupBy('olt_interface_gpon_ont.Id');
       $query->OrderBy('olt_interface_gpon_ont.Run','ASC');
       $query->OrderBy('olt_interface_gpon_ont.FrameId','ASC');
       $query->OrderBy('olt_interface_gpon_ont.SlotId','ASC');
       $query->OrderBy('olt_interface_gpon_ont.PortId','ASC');
       $query->OrderBy('olt_interface_gpon_ont.CreatedDate','ASC');
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select('olt_interface_gpon_ont.*','mst_customer_erp.name as Description')
                  ->leftjoin('mst_customer_noc',\DB::raw('CONCAT(mst_customer_noc.OntId,"-",mst_customer_noc.OntSn)'), '=', \DB::raw('CONCAT(olt_interface_gpon_ont.OntId,"-",olt_interface_gpon_ont.OntSn)'))
                  ->leftjoin('mst_customer_erp', 'mst_customer_noc.code', '=', 'mst_customer_noc.CustomerCode')
                  ->groupBy('olt_interface_gpon_ont.Id')
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotID' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntId' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'Flag' => 'nullable|max:250',
            'Run' => 'nullable|max:250',
            'Config' => 'nullable|max:250',
            'Match' => 'nullable|max:250',
            'Protect' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new InterfaceGpon();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->FrameId) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotID) {
            $data->SlotID = $request->SlotID;
        }
        if ($request->PortId) {
            $data->PortId = $request->PortId;
        }
        if ($request->OntId) {
            $data->OntId = $request->OntId;
        }
        if ($request->OntSn) {
            $data->OntSn = $request->OntSn;
        }
        if ($request->Flag) {
            $data->Flag = $request->Flag;
        }
        if ($request->Run) {
            $data->Run = $request->Run;
        }
        if ($request->Config) {
            $data->Config = $request->Config;
        }
        if ($request->Match) {
            $data->Match = $request->Match;
        }
        if ($request->Protect) {
            $data->Protect = $request->Protect;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
