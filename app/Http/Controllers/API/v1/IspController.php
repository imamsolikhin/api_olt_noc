<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\Isp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class IspController extends Controller
{

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        $query->select('*');
        $query->where('LinkId', '=', Null);
        if($request->search){
          $search = $request->search;
          $query->where(function($q) use($search) {
            $q->orWhere('mst_customer_erp.code', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.name', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.CompanyName', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.email', 'like', '%'.$search.'%');
          });
        }

        if ($request->ActiveStatus) {
            $query->where('ActiveStatus', '=', $request->ActiveStatus);
        }

        if($request->from_date != '' && $request->from_date  != ''){
          $query->whereBetween('mst_customer_erp.CreatedDate', array($request->from_date, $request->to_date)) ;
        }
        $query->orderBy('CreatedDate',"desc");

        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::select([
            '*'
        ])->find(str_replace('%20', ' ', $id));
        return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'LinkID' => 'nullable|max:250',
            'Name' => 'nullable|max:250',
            'ContactPerson' => 'nullable|max:250',
            'Address' => 'nullable|max:250',
            'CityCode' => 'nullable|max:250',
            'Phone1' => 'nullable|max:250',
            'Phone2' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'Email' => 'nullable|max:250',
            'ActiveStatus AS activeStatus',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Isp();
            $data->CreatedDate = currDate();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
            $data->MemberID = $request->Code;
        }
        if ($request->except("Name")) {
            $data->Name = $request->Name;
            $data->CompanyName = $request->Name;
        }
        if ($request->except("ContactPerson")) {
            $data->ContactPerson = $request->ContactPerson;
        }
        if ($request->except("Address")) {
            $data->Address = $request->Address;
        }
        if ($request->except("CityCode")) {
            $data->CityCode = $request->CityCode;
        }
        if ($request->except("Phone1")) {
            $data->Phone1 = $request->Phone1;
        }
        if ($request->except("Phone2")) {
            $data->Phone2 = $request->Phone2;
        }
        if ($request->except("Fax")) {
            $data->Fax = $request->Fax;
        }
        if ($request->except("Email")) {
            $data->Email = $request->Email;
        }
        if ($request->except("CreatedBy")) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except("CreatedDate")) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except("CreatedDate")) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except("UpdatedDate")) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }

        $data->UpdatedDate = currDate();
        $data->save();

        return $data;
    }
}
