<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\Clients;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class ClientsController extends Controller
{
    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
      public static function dashboard($request,$table)
      {
          $data['CountClientsActive'] = getModelName($table)::
                                        leftjoin('olt_interface_gpon_ont', 'olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc.OntSn')
                                        ->Join('olt_host', function($join){
                                            $join->on('olt_host.code', '=', 'mst_customer_noc.HostCode');
                                            $join->where('olt_host.ActiveStatus', '=', 1);
                                        })
                                        ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
                                        ->Join('mst_customer_erp', function($join){
                                            $join->on('mst_customer_erp.code', '=', 'mst_customer_noc.CustomerCode');
                                            $join->where('mst_customer_erp.ActiveStatus', '=', 1);
                                        })
                                        ->Join('mst_customer_erp AS mst_isp', function($join){
                                            $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                                            $join->where('mst_isp.ActiveStatus', '=', 1);
                                        })
                                        ->count();

          $data['CountClientsPostpone'] = getModelName($table)::where('ClientStatus','=','Postpone')->count();
          $data['CountClientsClosed'] = getModelName($table)::
                                        leftjoin('olt_interface_gpon_ont', 'olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc.OntSn')
                                        ->Join('olt_host', function($join){
                                            $join->on('olt_host.code', '=', 'mst_customer_noc.HostCode');
                                            $join->where('olt_host.ActiveStatus', '=', 0);
                                        })
                                        ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
                                        ->Join('mst_customer_erp', function($join){
                                            $join->on('mst_customer_erp.code', '=', 'mst_customer_noc.CustomerCode');
                                            $join->where('mst_customer_erp.ActiveStatus', '=', 0);
                                        })
                                        ->Join('mst_customer_erp AS mst_isp', function($join){
                                            $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                                            $join->where('mst_isp.ActiveStatus', '=', 0);
                                        })
                                        ->count();
          return $data;
      }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        $query->select([
            'mst_customer_noc.Code AS Code',
            'mst_customer_noc.HostCode AS HostCode',
            'olt_host.Hostname AS Hostname',
            'olt_host.IpAddress AS IpAddress',
            'olt_host.Username AS Username',
            'olt_host.Password AS Password',
            'olt_host.Port AS Port',
            'olt_host.Sysname AS Sysname',
            'olt_host.SnmpCommunity AS SnmpCommunity',
            'olt_host.ActiveStatus AS ActiveStatus',
            'olt_host.FrameId AS BoardFrameId',
            'olt_host.SlotId AS BoardSlotId',
            'olt_host.PortId AS BoardPortId',
            'mst_brand.Code AS DeviceCode',
            'mst_brand.Name AS DeviceName',
            'mst_brand.Model AS DeviceModel',
            'mst_brand.Type AS DeviceType',
            'mst_brand.Version AS DeviceVersion',
            'mst_isp.Code AS IspCode',
            'mst_isp.Name AS IspName',
            'mst_isp.ContactPerson AS IspContactPerson',
            'mst_isp.Address AS IspAddress',
            'mst_isp.CityCode AS IspCity',
            'mst_isp.phone1 AS IspPhone1',
            'mst_isp.phone2 AS IspPhone2',
            'mst_isp.Fax AS IspFax',
            'mst_isp.Email AS IspEmail',
            'mst_customer_erp.Code AS CustomerCode',
            'mst_customer_erp.Name AS CustomerName',
            'mst_customer_erp.ContactPerson AS CustomerContactPerson',
            'mst_customer_erp.Address AS CustomerAddress',
            'mst_customer_erp.CityCode AS CustomerCity',
            'mst_customer_erp.phone1 AS CustomerPhone1',
            'mst_customer_erp.phone2 AS CustomerPhone2',
            'mst_customer_erp.Fax AS CustomerFax',
            'mst_customer_erp.Email AS CustomerEmail',
            'mst_customer_erp.ProductGrup AS CustomerProductGrup',
            'mst_customer_erp.ProductPort AS CustomerProductPort',
            'mst_customer_erp.ProductCategory AS CustomerProductCategory',
            'mst_customer_erp.ProductName AS CustomerProductName',
            'mst_customer_erp.ProductNotes AS CustomerProductNotes',
            'mst_customer_noc.TemplateCode AS TemplateCode',
            'mst_customer_noc.VlanDownLink AS VlanDownLink',
            'mst_customer_noc.FrameId AS FrameId',
            'mst_customer_noc.SlotId AS SlotId',
            'mst_customer_noc.PortId AS PortId',
            'mst_customer_noc.OntVersion AS OntVersion',
            'mst_customer_noc.OntSoftware AS OntSoftware',
            'mst_customer_noc.OntSn AS OntSn',
            'mst_customer_noc.OntId AS OntId',
            'mst_customer_noc.ClientStatus AS ClientStatus',
            'mst_customer_noc.OntLineProfileId AS OntLineProfileId',
            'mst_customer_noc.OntSrvProfileId AS OntSrvProfileId',
            'mst_customer_noc.VlanId AS VlanId',
            'mst_customer_noc.VlanFrameId AS VlanFrameId',
            'mst_customer_noc.VlanSlotId AS VlanSlotId',
            'mst_customer_noc.VlanPortId AS VlanPortId',
            'mst_customer_noc.FdtNo AS FdtNo',
            'mst_customer_noc.FatNo AS FatNo',
            'mst_customer_noc.VlanAttribut AS VlanAttribut',
            'mst_customer_noc.NativeVlanEth1 AS NativeVlanEth1',
            'mst_customer_noc.NativeVlanEth2 AS NativeVlanEth2',
            'mst_customer_noc.NativeVlanEth3 AS NativeVlanEth3',
            'mst_customer_noc.NativeVlanEth4 AS NativeVlanEth4',
            'mst_customer_noc.Remark AS Remark',
            'mst_customer_noc.Response AS Response',
            'mst_customer_noc.CreatedDate AS CreatedDate',
            'olt_interface_gpon_ont.Flag AS OntFlag',
            'olt_interface_gpon_ont.Config AS OntConfig',
            'olt_interface_gpon_ont.Run AS OntRun',
            'olt_interface_gpon_ont.Match AS OntMatch',
            'olt_interface_gpon_ont.Protect AS OntProtect'

        ]);
        $query->join('olt_host', function($join){
                            $join->on('olt_host.code', '=', 'mst_customer_noc.HostCode');
                            $join->where('olt_host.ActiveStatus', '=', 1);
                        });
        $query->leftjoin('olt_interface_gpon_ont', function($join){
                            $join->on('olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc.OntSn');
                            $join->on('olt_interface_gpon_ont.HostCode', '=', "olt_host.code");
                        });
        $query->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode');
        $query->leftjoin('mst_customer_erp', function($join){
                            $join->on('mst_customer_erp.code', '=', 'mst_customer_noc.CustomerCode');
                            // $join->where('mst_customer_erp.ActiveStatus', '=', 1);
                        });
        $query->leftjoin('mst_customer_erp AS mst_isp', function($join){
                            $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                            // $join->where('mst_isp.ActiveStatus', '=', 1);
                        });
        if ($request->ActiveStatus) {
            $query->where('mst_customer_noc.ActiveStatus', '=', $request->ActiveStatus);
        }

        if($request->from_date != '' && $request->from_date  != ''){
          $query->whereBetween('mst_customer_erp.CreatedDate', array($request->from_date, $request->to_date)) ;
        }

        if($request->HostCode){
          $query->where('olt_host.Code',$request->HostCode);
        }
        if($request->ClientStatus){
          $query->where('mst_customer_noc.ClientStatus',$request->ClientStatus);
        }
        if($request->search){
          $search = $request->search;
          $query->where(function($q) use($search) {
            $q->orWhere('mst_customer_noc.OntSn', 'like', '%'.$search.'%');
            $q->orWhere('olt_host.Code', 'like', '%'.$search.'%');
            $q->orWhere('olt_host.Hostname', 'like', '%'.$search.'%');
            $q->orWhere('olt_host.IpAddress', 'like', '%'.$search.'%');
            $q->orWhere('olt_host.SnmpCommunity', 'like', '%'.$search.'%');
            $q->orWhere('mst_brand.code', 'like', '%'.$search.'%');
            $q->orWhere('mst_brand.name', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_noc.code', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.name', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.LinkID', 'like', '%'.$search.'%');
          });
        }
        $query->groupBy('mst_customer_noc.Code');
        $query->orderBy('mst_customer_noc.CreatedDate','DESC');
        $query->orderBy('mst_customer_noc.HostCode','ASC');
        $query->orderBy('mst_customer_noc.Code','ASC');
        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::select([
            'mst_customer_noc.Code AS Code',
            'mst_customer_noc.HostCode AS HostCode',
            'olt_host.Hostname AS Hostname',
            'olt_host.IpAddress AS IpAddress',
            'olt_host.Username AS Username',
            'olt_host.Password AS Password',
            'olt_host.Port AS Port',
            'olt_host.Sysname AS Sysname',
            'olt_host.SnmpCommunity AS SnmpCommunity',
            'olt_host.FrameId AS BoardFrameId',
            'olt_host.SlotId AS BoardSlotId',
            'olt_host.PortId AS BoardPortId',
            'olt_host.ActiveStatus AS ActiveStatus',
            'mst_brand.Code AS DeviceCode',
            'mst_brand.Name AS DeviceName',
            'mst_brand.Model AS DeviceModel',
            'mst_brand.Type AS DeviceType',
            'mst_brand.Version AS DeviceVersion',
            'mst_isp.Code AS IspCode',
            'mst_isp.Name AS IspName',
            'mst_isp.ContactPerson AS IspContactPerson',
            'mst_isp.Address AS IspAddress',
            'mst_isp.CityCode AS IspCity',
            'mst_isp.phone1 AS IspPhone1',
            'mst_isp.phone2 AS IspPhone2',
            'mst_isp.Fax AS IspFax',
            'mst_isp.Email AS IspEmail',
            'mst_customer_erp.Code AS CustomerCode',
            'mst_customer_erp.Name AS CustomerName',
            'mst_customer_erp.ContactPerson AS CustomerContactPerson',
            'mst_customer_erp.Address AS CustomerAddress',
            'mst_customer_erp.CityCode AS CustomerCity',
            'mst_customer_erp.phone1 AS CustomerPhone1',
            'mst_customer_erp.phone2 AS CustomerPhone2',
            'mst_customer_erp.Fax AS CustomerFax',
            'mst_customer_erp.Email AS CustomerEmail',
            'mst_customer_erp.ProductGrup AS CustomerProductGrup',
            'mst_customer_erp.ProductPort AS CustomerProductPort',
            'mst_customer_erp.ProductCategory AS CustomerProductCategory',
            'mst_customer_erp.ProductName AS CustomerProductName',
            'mst_customer_erp.ProductNotes AS CustomerProductNotes',
            'mst_customer_noc.TemplateCode AS TemplateCode',
            'mst_customer_noc.VlanDownLink AS VlanDownLink',
            'mst_customer_noc.FrameId AS FrameId',
            'mst_customer_noc.SlotId AS SlotId',
            'mst_customer_noc.PortId AS PortId',
            'mst_customer_noc.OntVersion AS OntVersion',
            'mst_customer_noc.OntSoftware AS OntSoftware',
            'mst_customer_noc.OntSn AS OntSn',
            'mst_customer_noc.OntId AS OntId',
            'mst_customer_noc.ClientStatus AS ClientStatus',
            'mst_customer_noc.OntLineProfileId AS OntLineProfileId',
            'mst_customer_noc.OntSrvProfileId AS OntSrvProfileId',
            'mst_customer_noc.VlanId AS VlanId',
            'mst_customer_noc.VlanFrameId AS VlanFrameId',
            'mst_customer_noc.VlanSlotId AS VlanSlotId',
            'mst_customer_noc.VlanPortId AS VlanPortId',
            'mst_customer_noc.FdtNo AS FdtNo',
            'mst_customer_noc.FatNo AS FatNo',
            'mst_customer_noc.VlanAttribut AS VlanAttribut',
            'mst_customer_noc.NativeVlanEth1 AS NativeVlanEth1',
            'mst_customer_noc.NativeVlanEth2 AS NativeVlanEth2',
            'mst_customer_noc.NativeVlanEth3 AS NativeVlanEth3',
            'mst_customer_noc.NativeVlanEth4 AS NativeVlanEth4',
            'mst_customer_noc.Remark AS Remark',
            'mst_customer_noc.Response AS Response',
            'mst_customer_noc.CreatedDate AS CreatedDate',
            'mst_customer_noc.UpdatedDate AS UpdatedDate',
            'mst_customer_noc_history.OntSn AS HistoryOntSn',
            'mst_customer_noc_history.Remark AS HistoryRemark',
            'mst_customer_noc_history.ClientStatus AS HistoryStatus',
            'mst_customer_noc_history.CreatedDate AS HistoryDate',
            'olt_interface_gpon_ont.Flag AS OntFlag',
            'olt_interface_gpon_ont.Config AS OntConfig',
            'olt_interface_gpon_ont.Run AS OntRun',
            'olt_interface_gpon_ont.Match AS OntMatch',
            'olt_interface_gpon_ont.Protect AS OntProtect'
        ])
          ->leftjoin('olt_host', function($join){
                              $join->on('olt_host.code', '=', 'mst_customer_noc.HostCode');
                              $join->where('olt_host.ActiveStatus', '=', 1);
                          })
          ->leftjoin('olt_interface_gpon_ont', function($join){
                            $join->on('olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc.OntSn');
                            $join->on('olt_interface_gpon_ont.HostCode', '=', "olt_host.code");
                        })
          ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
          ->leftJoin('mst_customer_erp', function($join){
                              $join->on('mst_customer_erp.code', '=', 'mst_customer_noc.CustomerCode');
                              $join->where('mst_customer_erp.ActiveStatus', '=', 1);
                          })
          ->leftJoin('mst_customer_erp AS mst_isp', function($join){
                              $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                              $join->where('mst_isp.ActiveStatus', '=', 1);
                          })
          ->leftJoin('mst_customer_noc_history', 'mst_customer_noc_history.CustomerCode', '=', DB::raw('(SELECT mst_customer_noc_history.CustomerCode FROM mst_customer_noc_history WHERE mst_customer_noc_history.CustomerCode = mst_customer_noc.CustomerCode ORDER BY mst_customer_noc_history.CreatedDate DESC LIMIT 1)'))
          ->orderBy('mst_customer_noc.CreatedDate','DESC')
          ->find(str_replace('%20', ' ', $id));
        return $data;
    }

    public static function setting($table, $id, $request)
    {
        $clients = getModelName($table)::select([
            'mst_customer_noc.Code AS Code',
            'mst_customer_noc.HostCode AS HostCode',
            'olt_host.Hostname AS Hostname',
            'olt_host.IpAddress AS IpAddress',
            'olt_host.Username AS Username',
            'olt_host.Password AS Password',
            'olt_host.Port AS Port',
            'olt_host.Sysname AS Sysname',
            'olt_host.SnmpCommunity AS SnmpCommunity',
            'olt_host.FrameId AS BoardFrameId',
            'olt_host.SlotId AS BoardSlotId',
            'olt_host.PortId AS BoardPortId',
            'olt_host.ActiveStatus AS ActiveStatus',
            'mst_brand.Code AS DeviceCode',
            'mst_brand.Name AS DeviceName',
            'mst_brand.Model AS DeviceModel',
            'mst_brand.Type AS DeviceType',
            'mst_brand.Version AS DeviceVersion',
            'mst_isp.Code AS IspCode',
            'mst_isp.Name AS IspName',
            'mst_isp.ContactPerson AS IspContactPerson',
            'mst_isp.Address AS IspAddress',
            'mst_isp.CityCode AS IspCity',
            'mst_isp.phone1 AS IspPhone1',
            'mst_isp.phone2 AS IspPhone2',
            'mst_isp.Fax AS IspFax',
            'mst_isp.Email AS IspEmail',
            'mst_customer_erp.Code AS CustomerCode',
            'mst_customer_erp.Name AS CustomerName',
            'mst_customer_erp.ContactPerson AS CustomerContactPerson',
            'mst_customer_erp.Address AS CustomerAddress',
            'mst_customer_erp.CityCode AS CustomerCity',
            'mst_customer_erp.phone1 AS CustomerPhone1',
            'mst_customer_erp.phone2 AS CustomerPhone2',
            'mst_customer_erp.Fax AS CustomerFax',
            'mst_customer_erp.Email AS CustomerEmail',
            'mst_customer_erp.ProductGrup AS CustomerProductGrup',
            'mst_customer_erp.ProductPort AS CustomerProductPort',
            'mst_customer_erp.ProductCategory AS CustomerProductCategory',
            'mst_customer_erp.ProductName AS CustomerProductName',
            'mst_customer_erp.ProductNotes AS CustomerProductNotes',
            'mst_customer_noc.TemplateCode AS TemplateCode',
            'mst_customer_noc.VlanDownLink AS VlanDownLink',
            'mst_customer_noc.FrameId AS FrameId',
            'mst_customer_noc.SlotId AS SlotId',
            'mst_customer_noc.PortId AS PortId',
            'mst_customer_noc.OntVersion AS OntVersion',
            'mst_customer_noc.OntSoftware AS OntSoftware',
            'mst_customer_noc.OntSn AS OntSn',
            'mst_customer_noc.OntId AS OntId',
            'mst_customer_noc.ClientStatus AS ClientStatus',
            'mst_customer_noc.OntLineProfileId AS OntLineProfileId',
            'mst_customer_noc.OntSrvProfileId AS OntSrvProfileId',
            'mst_customer_noc.VlanId AS VlanId',
            'mst_customer_noc.VlanFrameId AS VlanFrameId',
            'mst_customer_noc.VlanSlotId AS VlanSlotId',
            'mst_customer_noc.VlanPortId AS VlanPortId',
            'mst_customer_noc.FdtNo AS FdtNo',
            'mst_customer_noc.FatNo AS FatNo',
            'mst_customer_noc.VlanAttribut AS VlanAttribut',
            'mst_customer_noc.NativeVlanEth1 AS NativeVlanEth1',
            'mst_customer_noc.NativeVlanEth2 AS NativeVlanEth2',
            'mst_customer_noc.NativeVlanEth3 AS NativeVlanEth3',
            'mst_customer_noc.NativeVlanEth4 AS NativeVlanEth4',
            'mst_customer_noc.Remark AS Remark',
            'mst_customer_noc.Response AS Response',
            'mst_customer_noc.UpdatedDate AS UpdatedDate',
            'mst_customer_noc_history.OntSn AS HistoryOntSn',
            'mst_customer_noc_history.Remark AS HistoryRemark',
            'mst_customer_noc_history.ClientStatus AS HistoryStatus',
            'mst_customer_noc_history.CreatedDate AS HistoryDate',
            'olt_interface_gpon_ont.Flag AS OntFlag',
            'olt_interface_gpon_ont.Config AS OntConfig',
            'olt_interface_gpon_ont.Run AS OntRun',
            'olt_interface_gpon_ont.Match AS OntMatch',
            'olt_interface_gpon_ont.Protect AS OntProtect',
            DB::raw(' "" AS LastDownCause'),
            DB::raw(' "" AS LastUpTime'),
            DB::raw(' "" AS LastDownTime'),
            DB::raw(' "" AS LastDyingGasp'),
            DB::raw(' "" AS OntOnlineDuration'),
            DB::raw(' "" AS OntRx'),
            DB::raw(' "" AS OntRxPower'),
            DB::raw(' "" AS OntDistance'),
            DB::raw(' "" AS ServiceType'),
            DB::raw(' "" AS ConnectionType'),
            DB::raw(' "" AS Ipv4ConnectionStatus'),
            DB::raw(' "" AS PortState'),
            DB::raw(' "" AS StatusPort')

        ])
          ->leftjoin('olt_interface_gpon_ont', 'olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc.OntSn')
          ->leftJoin('olt_host', function($join){
                              $join->on('olt_host.code', '=', 'mst_customer_noc.HostCode');
                              $join->where('olt_host.ActiveStatus', '=', 1);
                          })
          ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
          ->leftJoin('mst_customer_erp', function($join){
                              $join->on('mst_customer_erp.code', '=', 'mst_customer_noc.CustomerCode');
                              $join->where('mst_customer_erp.ActiveStatus', '=', 1);
                          })
          ->leftJoin('mst_customer_erp AS mst_isp', function($join){
                              $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                              $join->where('mst_isp.ActiveStatus', '=', 1);
                          })

          ->leftJoin('mst_customer_noc_history', 'mst_customer_noc_history.CustomerCode', '=', DB::raw('(SELECT mst_customer_noc_history.CustomerCode FROM mst_customer_noc_history WHERE mst_customer_noc_history.CustomerCode = mst_customer_noc.CustomerCode ORDER BY mst_customer_noc_history.CreatedDate DESC LIMIT 1)'))
          ->orderBy('mst_customer_noc.CreatedDate','DESC')
          ->find(str_replace('%20', ' ', $id));

        if($clients->OntId == null){
          $clients->OntId = static::getOntIndex($clients->HostCode,$clients->FrameId,$clients->SlotId,$clients->PortId,0);

        }
        $data['clients'] = $clients;

        $rs = getModelName('ClientsVlanService')::select('*')
                ->leftJoin('olt_port_vlan', function($join){
                  $join->on('mst_customer_noc_service.code', '=', 'olt_port_vlan.HostCode');
                  $join->on('olt_port_vlan.Index', '=', 'olt_port_vlan.HostCode');
                })
                ->where('CustomerCode', $clients->CustomerCode)
                ->get();

        $service = [];
        $s=0;
        $g=0;

        $data['vlan'] = false;
        foreach ($rs as $value) {
          if($value->GemPort == null){
            $gem = getModelName('GemProfile')::select('*')
                    ->where('HostCode', $clients->HostCode)
                    ->where('ProfileId', $clients->OntLineProfileId)
                    ->where('GemVlan', $value->UserVlan)
                    ->first();
            if($gem == null){
              $value->GemPort = static::getGemIndex($clients->HostCode,$clients->OntLineProfileId,$g);
              $g = $value->GemPort+1;
              $value->ActiveStatus=1;
            }else{
              $value->GemPort = $gem->GemId;
              $g = $value->GemPort;
              $value->ActiveStatus=0;
            }
          }
          $last = (empty($srv))? 0 : $srv[count($srv)-1]+1;
          if($value->ServicePortId == null){
            $value->ServicePortId = static::getServicePortIndex($clients->HostCode,$s);
            $s = $value->ServicePortId+1;
          }
          array_push($service,$value);

          $vlan = getModelName('PortVlan')::select('*')
                  ->where('HostCode', $clients->HostCode)
                  ->where('Index', $value->Vlan)
                  ->count();
          // if($vlan){
          //   $data['vlan'] = true;
          // }
        }
        $data['service'] = $service;

        $ont = getModelName('InterfaceGpon')::select('*')
                ->where('HostCode', $clients->HostCode)
                ->where('OntId', $clients->OntId)
                ->count();
        $data['InterfaceGpon'] = false;
        if($ont){
          $data['InterfaceGpon'] = true;
        }

        $srv = getModelName('SrvProfile')::select('*')
                ->where('HostCode', $clients->HostCode)
                ->where('ProfileId', $clients->OntSrvProfileId)
                ->count();
        $data['srvProfile'] = false;
        if(!$srv){
          $data['srvProfile'] = true;
        }

        $srvPort = getModelName('SrvPort')::select('*')
                ->where('HostCode', $clients->HostCode)
                ->where('VlanAttr', $clients->VlanAttr)
                ->where('VlanId', $clients->VlanId)
                ->count();
        $data['srvPort'] = false;
        if($srvPort){
          $data['srvPort'] = true;
        }
        //
        $ln = getModelName('LineProfile')::select('*')
                ->where('HostCode', $clients->HostCode)
                ->where('ProfileId', $clients->OntLineProfileId)
                ->count();
        $data['lnprofile'] = false;
        if($ln){
          $data['lnprofile'] = true;
        }
        // $srvport getModelName('SrvPort')::select('*')
        // ->
        return $data;

    }

    public static function getGemIndex($HostCode,$OntLineProfileId,$i = 0){
        $val = null;
        for ($i; $i < 1000; $i++) {
          $gem = getModelName('GemProfile')::select('*')
                  ->where('HostCode', $HostCode)
                  ->where('ProfileId', $OntLineProfileId)
                  ->where('GemId', $i)
                  ->first();
          if($gem == null){
              $val = $i;
              break;
          }
        }
        return $val;
    }

    public static function getServicePortIndex($HostCode,$i = 0){
        $val = null;
        for ($i; $i < 1000; $i++) {
          $srv = getModelName('SrvPort')::select('*')
                  ->where('HostCode', $HostCode)
                  ->where('Index', $i)
                  ->first();
          if($srv == null){
              $val = $i;
              break;
          }
        }
        return $val;
    }

    public static function getOntIndex($HostCode,$FrameId,$SlotId,$PortId,$i = 0){
      $val  = null;
      for ($i; $i < 1000; $i++) {
        $ig = getModelName('InterfaceGpon')::select('*')
                ->where('HostCode', $HostCode)
                ->where('FrameId', $FrameId)
                ->where('SlotId', $SlotId)
                ->where('PortId', $PortId)
                ->where('OntId', $i)
                ->first();
          if($ig == null) {
               $val = $i;
               break;
          }
      }
      return $val;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Code' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntVersion' => 'nullable|max:250',
            'OntSoftware' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'OntId' => 'nullable|max:250',
            'OntRun' => 'nullable|max:250',
            'ClientStatus' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'VlanId' => 'nullable|max:250',
            'VlanFrameId' => 'nullable|max:250',
            'VlanSlotId' => 'nullable|max:250',
            'VlanPortId' => 'nullable|max:250',
            'FdtNo' => 'nullable|max:250',
            'FatNo' => 'nullable|max:250',
            'VlanAttribut' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'ServiceType' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Clients();
            $data->CreatedDate = currDate();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->CustomerCode) {
            $data->CustomerCode = $request->CustomerCode;
        }
        if ($request->TemplateCode) {
            $data->TemplateCode = $request->TemplateCode;
        }
        if ($request->FrameId or $request->FrameId == 0) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId or $request->SlotId == 0) {
            $data->SlotId = $request->SlotId;
        }
        if ($request->PortId or $request->PortId == 0) {
            $data->PortId = $request->PortId;
        }
        if ($request->OntVersion) {
            $data->OntVersion = $request->OntVersion;
        }
        if ($request->OntSoftware) {
            $data->OntSoftware = $request->OntSoftware;
        }
        if ($request->OntId or $request->OntId == 0) {
            $data->OntId = $request->OntId;
        }
        if ($request->OntSn) {
            $data->OntSn = $request->OntSn;
        }
        if ($request->ClientStatus) {
            $data->ClientStatus = $request->ClientStatus;
        }
        if ($request->OntLineProfileId or $request->OntLineProfileId == 0) {
            $data->OntLineProfileId = $request->OntLineProfileId;
        }
        if ($request->OntSrvProfileId or $request->OntSrvProfileId == 0) {
            $data->OntSrvProfileId = $request->OntSrvProfileId;
        }
        if ($request->VlanId or $request->VlanId == 0) {
            $data->VlanId = $request->VlanId;
        }
        if ($request->VlanFrameId or $request->VlanFrameId == 0) {
            $data->VlanFrameId = $request->VlanFrameId;
        }
        if ($request->VlanSlotId or $request->VlanSlotId == 0) {
            $data->VlanSlotId = $request->VlanSlotId;
        }
        if ($request->VlanPortId or $request->VlanPortId == 0) {
            $data->VlanPortId = $request->VlanPortId;
        }
        if ($request->FdtNo) {
            $data->FdtNo = $request->FdtNo;
        }
        if ($request->FatNo) {
            $data->FatNo = $request->FatNo;
        }
        if ($request->VlanAttribut) {
            $data->VlanAttribut = $request->VlanAttribut;
        }
        if ($request->VlanDownLink or $request->VlanDownLink == 0) {
            $data->VlanDownLink = $request->VlanDownLink;
        }
        if ($request->NativeVlanEth1 or $request->NativeVlanEth1 == 0) {
            $data->NativeVlanEth1 = $request->NativeVlanEth1;
        }
        if ($request->NativeVlanEth2 or $request->NativeVlanEth2 == 0) {
            $data->NativeVlanEth2 = $request->NativeVlanEth2;
        }
        if ($request->NativeVlanEth3 or $request->NativeVlanEth3 == 0) {
            $data->NativeVlanEth3 = $request->NativeVlanEth3;
        }
        if ($request->NativeVlanEth4 or $request->NativeVlanEth4 == 0) {
            $data->NativeVlanEth4 = $request->NativeVlanEth4;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->Response){
            $data->Response = $request->Response;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->UpdatedDate = currDate();
        $data->save();

        return $data;
    }
}
