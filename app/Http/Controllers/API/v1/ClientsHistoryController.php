<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\ClientsHistory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClientsHistoryController extends Controller
{

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        $query->select([
            'mst_customer_noc_history.Code AS Code',
            'mst_customer_noc_history.HostCode AS HostCode',
            'olt_host.Hostname AS Hostname',
            'olt_host.IpAddress AS IpAddress',
            'olt_host.Username AS Username',
            'olt_host.Password AS Password',
            'olt_host.Port AS Port',
            'olt_host.Sysname AS Sysname',
            'olt_host.SnmpCommunity AS SnmpCommunity',
            'olt_host.ActiveStatus AS ActiveStatus',
            'olt_host.FrameId AS BoardFrameId',
            'olt_host.SlotId AS BoardSlotId',
            'olt_host.PortId AS BoardPortId',
            'mst_brand.Code AS DeviceCode',
            'mst_brand.Name AS DeviceName',
            'mst_brand.Model AS DeviceModel',
            'mst_brand.Type AS DeviceType',
            'mst_brand.Version AS DeviceVersion',
            'mst_isp.Code AS IspCode',
            'mst_isp.Name AS IspName',
            'mst_isp.ContactPerson AS IspContactPerson',
            'mst_isp.Address AS IspAddress',
            'mst_isp.CityCode AS IspCity',
            'mst_isp.phone1 AS IspPhone1',
            'mst_isp.phone2 AS IspPhone2',
            'mst_isp.Fax AS IspFax',
            'mst_isp.Email AS IspEmail',
            'mst_customer_erp.Code AS CustomerCode',
            'mst_customer_erp.Name AS CustomerName',
            'mst_customer_erp.ContactPerson AS CustomerContactPerson',
            'mst_customer_erp.Address AS CustomerAddress',
            'mst_customer_erp.CityCode AS CustomerCity',
            'mst_customer_erp.phone1 AS CustomerPhone1',
            'mst_customer_erp.phone2 AS CustomerPhone2',
            'mst_customer_erp.Fax AS CustomerFax',
            'mst_customer_erp.Email AS CustomerEmail',
            'mst_customer_erp.ProductGrup AS CustomerProductGrup',
            'mst_customer_erp.ProductPort AS CustomerProductPort',
            'mst_customer_erp.ProductCategory AS CustomerProductCategory',
            'mst_customer_erp.ProductName AS CustomerProductName',
            'mst_customer_erp.ProductNotes AS CustomerProductNotes',
            'mst_customer_noc_history.TemplateCode AS TemplateCode',
            'mst_customer_noc_history.VlanDownLink AS VlanDownLink',
            'mst_customer_noc_history.FrameId AS FrameId',
            'mst_customer_noc_history.SlotId AS SlotId',
            'mst_customer_noc_history.PortId AS PortId',
            'mst_customer_noc_history.OntVersion AS OntVersion',
            'mst_customer_noc_history.OntSoftware AS OntSoftware',
            'mst_customer_noc_history.OntSn AS OntSn',
            'mst_customer_noc_history.OntId AS OntId',
            'mst_customer_noc_history.Clientstatus AS Clientstatus',
            'mst_customer_noc_history.OntLineProfileId AS OntLineProfileId',
            'mst_customer_noc_history.OntSrvProfileId AS OntSrvProfileId',
            'mst_customer_noc_history.VlanId AS VlanId',
            'mst_customer_noc_history.VlanFrameId AS VlanFrameId',
            'mst_customer_noc_history.VlanSlotId AS VlanSlotId',
            'mst_customer_noc_history.VlanPortId AS VlanPortId',
            'mst_customer_noc_history.FdtNo AS FdtNo',
            'mst_customer_noc_history.FatNo AS FatNo',
            'mst_customer_noc_history.VlanAttribut AS VlanAttribut',
            'mst_customer_noc_history.NativeVlanEth1 AS NativeVlanEth1',
            'mst_customer_noc_history.NativeVlanEth2 AS NativeVlanEth2',
            'mst_customer_noc_history.NativeVlanEth3 AS NativeVlanEth3',
            'mst_customer_noc_history.NativeVlanEth4 AS NativeVlanEth4',
            'mst_customer_noc_history.Remark AS Remark',
            'mst_customer_noc_history.Response AS Response',
            'mst_customer_noc_history.ClientStatus AS ClientStatus',
            'mst_customer_noc_history.CreatedDate AS CreatedDate',
            'olt_interface_gpon_ont.Flag AS OntFlag',
            'olt_interface_gpon_ont.Config AS OntConfig',
            'olt_interface_gpon_ont.Run AS OntRun',
            'olt_interface_gpon_ont.Match AS OntMatch',
            'olt_interface_gpon_ont.Protect AS OntProtect'
        ]);
        $query->leftjoin('olt_interface_gpon_ont', 'olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc_history.OntSn');
        $query->leftjoin('olt_host', 'olt_host.code', '=', 'mst_customer_noc_history.HostCode');
        $query->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode');
        $query->leftjoin('mst_customer_erp', 'mst_customer_erp.code', '=', 'mst_customer_noc_history.CustomerCode');
        $query->leftjoin('mst_customer_erp AS mst_isp', 'mst_isp.code', '=', 'mst_customer_erp.LinkId');
        // if ($request->ActiveStatus) {
        //     $query->where('ActiveStatus', '=', $request->ActiveStatus);
        // }
        if ($request->CustomerCode) {
            $query->where('CustomerCode', '=', $request->CustomerCode);
        }
        $query->orderBy('mst_customer_noc_history.CreatedDate','DESC');
        // dd($query);
        if($request->from_date != '' && $request->from_date  != ''){
          $query->whereBetween('mst_customer_noc_history.CreatedDate', array($request->from_date, $request->to_date)) ;
        }
        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::select([
            'mst_customer_noc_history.Code AS Code',
            'mst_customer_noc_history.HostCode AS HostCode',
            'olt_host.Hostname AS Hostname',
            'olt_host.IpAddress AS IpAddress',
            'olt_host.Username AS Username',
            'olt_host.Password AS Password',
            'olt_host.Port AS Port',
            'olt_host.Sysname AS Sysname',
            'olt_host.SnmpCommunity AS SnmpCommunity',
            'olt_host.FrameId AS BoardFrameId',
            'olt_host.SlotId AS BoardSlotId',
            'olt_host.PortId AS BoardPortId',
            'olt_host.ActiveStatus AS ActiveStatus',
            'mst_brand.Code AS DeviceCode',
            'mst_brand.Name AS DeviceName',
            'mst_brand.Model AS DeviceModel',
            'mst_brand.Type AS DeviceType',
            'mst_brand.Version AS DeviceVersion',
            'mst_isp.Code AS IspCode',
            'mst_isp.Name AS IspName',
            'mst_isp.ContactPerson AS IspContactPerson',
            'mst_isp.Address AS IspAddress',
            'mst_isp.CityCode AS IspCity',
            'mst_isp.phone1 AS IspPhone1',
            'mst_isp.phone2 AS IspPhone2',
            'mst_isp.Fax AS IspFax',
            'mst_isp.Email AS IspEmail',
            'mst_customer_erp.Code AS CustomerCode',
            'mst_customer_erp.Name AS CustomerName',
            'mst_customer_erp.ContactPerson AS CustomerContactPerson',
            'mst_customer_erp.Address AS CustomerAddress',
            'mst_customer_erp.CityCode AS CustomerCity',
            'mst_customer_erp.phone1 AS CustomerPhone1',
            'mst_customer_erp.phone2 AS CustomerPhone2',
            'mst_customer_erp.Fax AS CustomerFax',
            'mst_customer_erp.Email AS CustomerEmail',
            'mst_customer_erp.ProductGrup AS CustomerProductGrup',
            'mst_customer_erp.ProductPort AS CustomerProductPort',
            'mst_customer_erp.ProductCategory AS CustomerProductCategory',
            'mst_customer_erp.ProductName AS CustomerProductName',
            'mst_customer_erp.ProductNotes AS CustomerProductNotes',
            'mst_customer_noc_history.TemplateCode AS TemplateCode',
            'mst_customer_noc_history.VlanDownLink AS VlanDownLink',
            'mst_customer_noc_history.FrameId AS FrameId',
            'mst_customer_noc_history.SlotId AS SlotId',
            'mst_customer_noc_history.PortId AS PortId',
            'mst_customer_noc_history.OntVersion AS OntVersion',
            'mst_customer_noc_history.OntSoftware AS OntSoftware',
            'mst_customer_noc_history.OntSn AS OntSn',
            'mst_customer_noc_history.OntId AS OntId',
            'mst_customer_noc_history.Clientstatus AS Clientstatus',
            'mst_customer_noc_history.OntLineProfileId AS OntLineProfileId',
            'mst_customer_noc_history.OntSrvProfileId AS OntSrvProfileId',
            'mst_customer_noc_history.VlanId AS VlanId',
            'mst_customer_noc_history.VlanFrameId AS VlanFrameId',
            'mst_customer_noc_history.VlanSlotId AS VlanSlotId',
            'mst_customer_noc_history.VlanPortId AS VlanPortId',
            'mst_customer_noc_history.FdtNo AS FdtNo',
            'mst_customer_noc_history.FatNo AS FatNo',
            'mst_customer_noc_history.VlanAttribut AS VlanAttribut',
            'mst_customer_noc_history.NativeVlanEth1 AS NativeVlanEth1',
            'mst_customer_noc_history.NativeVlanEth2 AS NativeVlanEth2',
            'mst_customer_noc_history.NativeVlanEth3 AS NativeVlanEth3',
            'mst_customer_noc_history.NativeVlanEth4 AS NativeVlanEth4',
            'mst_customer_noc_history.Remark AS Remark',
            'mst_customer_noc_history.Response AS Response',
            'mst_customer_noc_history.ClientStatus AS ClientStatus',
            'olt_interface_gpon_ont.Flag AS OntFlag',
            'olt_interface_gpon_ont.Config AS OntConfig',
            'olt_interface_gpon_ont.Run AS OntRun',
            'olt_interface_gpon_ont.Match AS OntMatch',
            'olt_interface_gpon_ont.Protect AS OntProtect'
        ])
            ->leftjoin('olt_interface_gpon_ont', 'olt_interface_gpon_ont.OntSn', '=', 'mst_customer_noc_history.OntSn')
            ->leftjoin('olt_host', 'olt_host.code', '=', 'mst_customer_noc_history.HostCode')
            ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
            ->leftjoin('mst_customer_erp', 'mst_customer_erp.code', '=', 'mst_customer_noc_history.CustomerCode')
            ->leftjoin('mst_customer_erp AS mst_isp', 'mst_isp.code', '=', 'mst_customer_erp.LinkId')
            ->orderBy('mst_customer_noc_history.CreatedDate','DESC')
            ->find(str_replace('%20', ' ', $id));
        return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Code' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntVersion' => 'nullable|max:250',
            'OntSoftware' => 'nullable|max:250',
            'OntSn' => 'nullable|max:250',
            'Clientstatus' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'VlanId' => 'nullable|max:250',
            'VlanFrameId' => 'nullable|max:250',
            'VlanSlotId' => 'nullable|max:250',
            'VlanPortId' => 'nullable|max:250',
            'FdtNo' => 'nullable|max:250',
            'FatNo' => 'nullable|max:250',
            'VlanAttribut' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'ServiceType' => 'nullable|max:250',
            'Remark' => 'nullable|max:250',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new ClientsHistory();
        }

        $data->Code = generadeCode($table,"FMI-HSTRY",$request->CustomerCode,5);
        // if ($request->Code) {
        //     $data->Code = $request->Code;
        // }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->CustomerCode) {
            $data->CustomerCode = $request->CustomerCode;
        }
        if ($request->TemplateCode) {
            $data->TemplateCode = $request->TemplateCode;
        }
        if ($request->VlanDownLink) {
            $data->VlanDownLink = $request->VlanDownLink;
        }
        if ($request->FrameId) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId) {
            $data->SlotId = $request->SlotId;
        }
        if ($request->PortId) {
            $data->PortId = $request->PortId;
        }
        if ($request->OntVersion) {
            $data->OntVersion = $request->OntVersion;
        }
        if ($request->OntSoftware) {
            $data->OntSoftware = $request->OntSoftware;
        }
        if ($request->OntSn) {
            $data->OntSn = $request->OntSn;
        }
        if ($request->Clientstatus) {
            $data->Clientstatus = $request->Clientstatus;
        }
        if ($request->OntLineProfileId) {
            $data->OntLineProfileId = $request->OntLineProfileId;
        }
        if ($request->OntSrvProfileId) {
            $data->OntSrvProfileId = $request->OntSrvProfileId;
        }
        if ($request->VlanId) {
            $data->VlanId = $request->VlanId;
        }
        if ($request->VlanFrameId) {
            $data->VlanFrameId = $request->VlanFrameId;
        }
        if ($request->VlanSlotId) {
            $data->VlanSlotId = $request->VlanSlotId;
        }
        if ($request->VlanPortId) {
            $data->VlanPortId = $request->VlanPortId;
        }
        if ($request->FdtNo) {
            $data->FdtNo = $request->FdtNo;
        }
        if ($request->FatNo) {
            $data->FatNo = $request->FatNo;
        }
        if ($request->VlanAttribut) {
            $data->VlanAttribut = $request->VlanAttribut;
        }
        if ($request->NativeVlanEth1) {
            $data->NativeVlanEth1 = $request->NativeVlanEth1;
        }
        if ($request->NativeVlanEth2) {
            $data->NativeVlanEth2 = $request->NativeVlanEth2;
        }
        if ($request->NativeVlanEth3) {
            $data->NativeVlanEth3 = $request->NativeVlanEth3;
        }
        if ($request->NativeVlanEth4) {
            $data->NativeVlanEth4 = $request->NativeVlanEth4;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->Remark) {
            $data->Remark = $request->Remark;
        }
        if ($request->ClientStatus) {
            $data->ClientStatus = $request->ClientStatus;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }
}
