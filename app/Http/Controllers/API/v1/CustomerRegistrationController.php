<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\CustomerRegistration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerRegistrationController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Code' => 'nullable|max:250',
            'CompanyCode' => 'nullable|max:250',
            'CompanyName' => 'nullable|max:250',
            'CompanyInisial' => 'nullable|max:250',
            'CompanyGroup' => 'nullable|max:250',
            'LineofBusiness' => 'nullable|max:250',
            'CustomerCode' => 'nullable|max:250',
            'CustomerCompanyCode' => 'nullable|max:250',
            'CustomerCompanyName' => 'nullable|max:250',
            'CustomerProvinceBirth' => 'nullable|max:250',
            'CustomerCityBirth' => 'nullable|max:250',
            'CustomerBirthDate' => 'nullable|max:250',
            'CustomerJobTitle' => 'nullable|max:250',
            'CustomerPhone' => 'nullable|max:250',
            'CustomerFax' => 'nullable|max:250',
            'CustomerEmail' => 'nullable|max:250',
            'CustomerPhoneNumber' => 'nullable|max:250',
            'CustomerNPWP' => 'nullable|max:250',
            'CustomerAddress' => 'nullable|max:250',
            'CustomerProvinceCode' => 'nullable|max:250',
            'CustomerProvinceName' => 'nullable|max:250',
            'CustomerCityCode' => 'nullable|max:250',
            'CustomerCityName' => 'nullable|max:250',
            'CustomerZipCode' => 'nullable|max:250',
            'CustomerWebsite' => 'nullable|max:250',
            'CustomerIDCard' => 'nullable|max:250',
            'CustomerIDCardNumber' => 'nullable|max:250',
            'CustomerIDCardExpired' => 'nullable|max:250',
            'CustomerProductSite' => 'nullable|max:250',
            'CustomerRegistrationDate' => 'nullable|max:250',
            'CustomerProductCode' => 'nullable|max:250',
            'CustomerStatusClient' => 'nullable|max:250',
            'CustomerSalesCode' => 'nullable|max:250',
            'CustomerSalesName' => 'nullable|max:250',
            'TechnicalPicCode' => 'nullable|max:250',
            'TechnicalPicCompanyCode' => 'nullable|max:250',
            'TechnicalPicCompanyName' => 'nullable|max:250',
            'TechnicalPicName' => 'nullable|max:250',
            'TechnicalPicDepartment' => 'nullable|max:250',
            'TechnicalPicJobTitle' => 'nullable|max:250',
            'TechnicalPicPhoneNumber' => 'nullable|max:250',
            'TechnicalPicFax' => 'nullable|max:250',
            'TechnicalPicEmail' => 'nullable|max:250',
            'TechnicalPicSalesInfo' => 'nullable|max:250',
            'TechnicalPicNIP' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new CustomerRegistration();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->Name) {
            $data->Name = $request->Name;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = $request->ActiveStatus;
        }
        $data->save();

        return $data;
    }

}
