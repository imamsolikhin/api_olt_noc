<?php
namespace App\Http\Controllers\API\v1;

use App\Http\Models\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
  /**
   * Inkombizz | inkombizz@gmail.com | inkombizz.com
   * Set Queries for Search.
   *
   * @param Illuminate\Database\Eloquent\Builder $query
   * @param string $key
   * @return Illuminate\Database\Eloquent\Builder
   */
  public static function dashboard($request,$table)
  {
      $query->select('*');
      if ($request->ActiveStatus) {
          $query->where('ActiveStatus', '=', $request->ActiveStatus);
      }
      $query->where('LinkID', '!=', Null);
      return $query;
  }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request)
    {
        $query->select('mst_customer_erp.*',
                        'mst_isp.Code AS IspCode',
                        'mst_isp.Name AS IspName',
                        'mst_isp.ContactPerson AS IspContactPerson',
                        'mst_isp.Address AS IspAddress',
                        'mst_isp.CityCode AS IspCity',
                        'mst_isp.phone1 AS IspPhone1',
                        'mst_isp.phone2 AS IspPhone2',
                        'mst_isp.Fax AS IspFax',
                        'mst_isp.Email AS IspEmail',);
        $query->leftjoin('mst_customer_erp AS mst_isp', function($join){
                    $join->on('mst_isp.code', '=', 'mst_customer_erp.LinkId');
                    $join->where('mst_isp.LinkId', '=', null);
                });
        $query->where('mst_customer_erp.LinkId', '!=', null);
        if ($request->ActiveStatus) {
            $query->where('mst_customer_noc.ActiveStatus', '=', $request->ActiveStatus);
        }

        if($request->from_date != '' && $request->from_date  != ''){
          $query->whereBetween('mst_customer_erp.CreatedDate', array($request->from_date, $request->to_date)) ;
        }

        if($request->search){
          $search = $request->search;
          $query->where(function($q) use($search) {
            $q->orWhere('mst_customer_erp.code', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.name', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.CompanyName', 'like', '%'.$search.'%');
            $q->orWhere('mst_customer_erp.email', 'like', '%'.$search.'%');
          });
        }
        // dd(getSql($query));
        return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @param string $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request)
    {
        $data = getModelName($table)::select([
            '*'
        ])->find(str_replace('%20', ' ', $id));
        return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Name' => 'nullable|max:250',
            'LinkID' => 'nullable|max:250',
            'MemberID' => 'nullable|max:250',
            'JobTitle' => 'nullable|max:250',
            'ContactPerson' => 'nullable|max:250',
            'Email' => 'nullable|max:250',
            'Phone1' => 'nullable|max:250',
            'Phone2' => 'nullable|max:250',
            'PhoneNumber' => 'nullable|max:250',
            'NPWP' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'Address' => 'nullable|max:250',
            'CityCode' => 'nullable|max:250',
            'City' => 'nullable|max:250',
            'Province' => 'nullable|max:250',
            'ZipCode' => 'nullable|max:250',
            'Website' => 'nullable|max:250',
            'IDCard' => 'nullable|max:250',
            'IDCardNumber' => 'nullable|max:250',
            'IDCardExpired' => 'nullable|max:250',
            'RegistrationDate' => 'nullable|max:250',
            'ProductCode' => 'nullable|max:250',
            'ClientStatus' => 'nullable|max:250',
            'ProductSite' => 'nullable|max:250',
            'ProductGrup' => 'nullable|max:250',
            'ProductPort' => 'nullable|max:250',
            'ProductCategory' => 'nullable|max:250',
            'BaseTransmissionStationCode' => 'nullable|max:250',
            'ProductName' => 'nullable|max:250',
            'ProductNotes' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:250' ,
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];
        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param object|null $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Customer();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
            $data->MemberID = $request->Code;
            $data->CreatedDate = currDate();
        } else {
          $code = generadeCode($table,$request->LinkID,null,6);
          $data->Code = $code;
          $data->MemberID = $code;
          $data->CreatedDate = currDate();
        }

        if ($request->except('Name')) {
            $data->Name = $request->Name;
        }
        if ($request->except('LinkID')) {
            $data->LinkID = $request->LinkID;
        }
        if ($request->except('JobTitle')) {
            $data->JobTitle = $request->JobTitle;
        }
        if ($request->except('ContactPerson')) {
            $data->ContactPerson = $request->ContactPerson;
        }
        if ($request->except('Email')) {
            $data->Email = $request->Email;
        }
        if ($request->except('Phone1')) {
            $data->Phone1 = $request->Phone1;
        }
        if ($request->except('Phone2')) {
            $data->Phone2 = $request->Phone2;
        }
        if ($request->except('PhoneNumber')) {
            $data->PhoneNumber = $request->PhoneNumber;
        }
        if ($request->except('NPWP')) {
            $data->NPWP = $request->NPWP;
        }
        if ($request->except('Fax')) {
            $data->Fax = $request->Fax;
        }
        if ($request->except('Address')) {
            $data->Address = $request->Address;
        }
        if ($request->except('CityCode')) {
            $data->CityCode = $request->CityCode;
        }
        if ($request->except('City')) {
            $data->City = $request->City;
        }
        if ($request->except('Province')) {
            $data->Province = $request->Province;
        }
        if ($request->except('ZipCode')) {
            $data->ZipCode = $request->ZipCode;
        }
        if ($request->except('Website')) {
            $data->Website = $request->Website;
        }
        if ($request->except('IDCard')) {
            $data->IDCard = $request->IDCard;
        }
        if ($request->except('IDCardNumber')) {
            $data->IDCardNumber = $request->IDCardNumber;
        }
        if ($request->except('IDCardExpired')) {
            $data->IDCardExpired = $request->IDCardExpired;
        }
        if ($request->except('RegistrationDate')) {
            $data->RegistrationDate = $request->RegistrationDate;
        }
        if ($request->except('ProductCode')) {
            $data->ProductCode = $request->ProductCode;
        }
        if ($request->except('ClientStatus')) {
            $data->ClientStatus = $request->ClientStatus;
        }
        if ($request->except('ProductSite')) {
            $data->ProductSite = $request->ProductSite;
        }
        if ($request->except('ProductGrup')) {
            $data->ProductGrup = $request->ProductGrup;
        }
        if ($request->except('ProductPort')) {
            $data->ProductPort = $request->ProductPort;
        }
        if ($request->except('ProductCategory')) {
            $data->ProductCategory = $request->ProductCategory;
        }
        if ($request->except('BaseTransmissionStationCode')) {
            $data->BaseTransmissionStationCode = $request->BaseTransmissionStationCode;
        }
        if ($request->except('ProductName')) {
            $data->ProductName = $request->ProductName;
        }
        if ($request->except('ProductNotes')) {
            $data->ProductNotes = $request->ProductNotes;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->except('UpdatedDate')) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }

        $data->UpdatedDate = currDate();
        $data->save();

        return $data;
    }
}
