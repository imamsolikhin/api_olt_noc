<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\SrvPort;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SrvPortController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       $query->where('HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       $query->OrderBy('FrameId','ASC');
       $query->OrderBy('SlotId','ASC');
       $query->OrderBy('PortId','ASC');
       // $query->select([
       //   'olt_service_port.Index AS Index',
       //   'olt_service_port.VlanId AS VlanId',
       //   'olt_service_port.VlanAttr AS VlanAttr',
       //   'olt_service_port.Type AS Type',
       //   'olt_service_port.FrameId AS FrameId',
       //   'olt_service_port.SlotId AS SlotId',
       //   'olt_service_port.PortId AS PortId',
       //   'olt_service_port.VPI AS VPI',
       //   'olt_service_port.VCI AS VCI',
       //   'olt_service_port.FlowType AS FlowType',
       //   'olt_service_port.FlowPara AS FlowPara',
       //   'olt_service_port.Rx AS Rx',
       //   'olt_service_port.Tx AS Tx',
       //   'olt_service_port.State AS State',
       //   'olt_service_port.Transparent AS Transparent',
       //   'mst_customer_noc_service.ServicePortId AS ServicePortId',
       //   'mst_customer_erp.Code AS CustomerCode',
       //   'mst_customer_erp.Name AS CustomerName'
       // ]);
       // $query->leftjoin('mst_customer_noc_service','mst_customer_noc_service.ServicePortId', '=', 'olt_service_port.Index');
       // $query->leftjoin('mst_customer_erp','mst_customer_erp.Code', '=', 'mst_customer_noc_service.CustomerCode');
       // if($request->ActiveStatus){
       //     $query->where('olt_service_port.ActiveStatus', '=', $request->ActiveStatus);
       // }
       // if($request->CustomerCode){
       //     $query->where('mst_customer_erp.Code', '=', $request->CustomerCode);
       // }
       // $query->where('olt_service_port.HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'HostCode' => 'nullable|max:250',
            'Index' => 'nullable|max:250',
            'VlanId' => 'nullable|max:250',
            'VlanAttr' => 'nullable|max:250',
            'Type' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'VPI' => 'nullable|max:250',
            'VCI' => 'nullable|max:250',
            'FlowType' => 'nullable|max:250',
            'FlowPara' => 'nullable|max:250',
            'Rx' => 'nullable|max:250',
            'Tx' => 'nullable|max:250',
            'State' => 'nullable|max:250',
            'Transparent' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new SrvPort();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->Index) {
            $data->Index = $request->Index;
        }
        if ($request->VlanId) {
            $data->VlanId = $request->VlanId;
        }
        if ($request->VlanAttr) {
            $data->VlanAttr = $request->VlanAttr;
        }
        if ($request->Fix) {
            $data->Fix = $request->Fix;
        }
        if ($request->Type) {
            $data->Type = $request->Type;
        }
        if ($request->FrameId) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId) {
            $data->SlotId = $request->SlotId;
        }
        if ($request->PortId) {
            $data->PortId = $request->PortId;
        }
        if ($request->VPI) {
            $data->VPI = $request->VPI;
        }
        if ($request->VCI) {
            $data->VCI = $request->VCI;
        }
        if ($request->FlowType) {
            $data->FlowType = $request->FlowType;
        }
        if ($request->FlowPara) {
            $data->FlowPara = $request->FlowPara;
        }
        if ($request->Rx) {
            $data->Rx = $request->Rx;
        }
        if ($request->Tx) {
            $data->Tx = $request->Tx;
        }
        if ($request->State) {
            $data->State = $request->State;
        }
        if ($request->Transparent) {
            $data->Transparent = $request->Transparent;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
