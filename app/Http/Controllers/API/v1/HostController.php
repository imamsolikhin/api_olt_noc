<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\Host;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class HostController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
        $query->select([
                    'olt_host.Code AS Code',
                    'olt_host.Hostname AS Hostname',
                    'olt_host.IpAddress AS IpAddress',
                    'olt_host.Username AS Username',
                    'olt_host.Password AS Password',
                    'olt_host.Port AS Port',
                    'olt_host.FrameId AS FrameId',
                    'olt_host.SlotId AS SlotId',
                    'olt_host.PortId AS PortId',
                    'olt_host.Sysname AS Sysname',
                    'olt_host.SnmpCommunity AS SnmpCommunity',
                    'olt_host.ActiveStatus AS ActiveStatus',
                    'olt_host.Remark AS Remark',
                    'olt_host.CurrentConfig AS CurrentConfig',
                    'mst_base_transmission_station.Code AS BtsCode',
                    'mst_base_transmission_station.Name AS BtsName',
                    'mst_base_transmission_station.Address AS BtsAddress',
                    'mst_brand.Code AS DeviceCode',
                    'mst_brand.Name AS DeviceName',
                    'mst_brand.Model AS DeviceModel',
                    'mst_brand.Type AS DeviceType',
                    'mst_brand.Version AS DeviceVersion',
                    'CheckStatus AS CheckStatus',
                    'CheckDate AS CheckDate',
                    DB::raw('GROUP_CONCAT(olt_line_profile.ProfileId) AS ProfileId'),
        ]);
       $query->leftjoin('olt_line_profile', 'olt_line_profile.HostCode', '=', 'olt_host.code');
       $query->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode');
       $query->leftjoin('mst_base_transmission_station', 'mst_base_transmission_station.code', '=', 'olt_host.BtsCode');

       if($request->ActiveStatus){
           $query->where('olt_host.ActiveStatus', '=', $request->ActiveStatus);
       }
       if($request->from_date != '' && $request->from_date  != ''){
         $query->whereBetween('olt_host.CreatedDate', array($request->from_date, $request->to_date)) ;
       }
       if($request->search){
           $query->orWhere(DB::raw('olt_host.Code'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.Hostname'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.IpAddress'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.Username'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.Password'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.Port'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.FrameId'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.SlotId'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.PortId'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.Sysname'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('olt_host.SnmpCommunity'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('mst_base_transmission_station.code'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('mst_base_transmission_station.name'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('mst_brand.code'), 'like', '%'.$request->search.'%');
           $query->orWhere(DB::raw('mst_brand.name'), 'like', '%'.$request->search.'%');
       }
       $query->groupBy('olt_host.Code');
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    'olt_host.Code AS Code',
                    'olt_host.Hostname AS Hostname',
                    'olt_host.IpAddress AS IpAddress',
                    'olt_host.Username AS Username',
                    'olt_host.Password AS Password',
                    'olt_host.Port AS Port',
                    'olt_host.FrameId AS FrameId',
                    'olt_host.SlotId AS SlotId',
                    'olt_host.PortId AS PortId',
                    'olt_host.Sysname AS Sysname',
                    'olt_host.SnmpCommunity AS SnmpCommunity',
                    'olt_host.ActiveStatus AS ActiveStatus',
                    'olt_host.Remark AS Remark',
                    'olt_host.CurrentConfig AS CurrentConfig',
                    'mst_base_transmission_station.Code AS BtsCode',
                    'mst_base_transmission_station.Name AS BtsName',
                    'mst_base_transmission_station.Address AS BtsAddress',
                    'mst_brand.Code AS DeviceCode',
                    'mst_brand.Name AS DeviceName',
                    'mst_brand.Model AS DeviceModel',
                    'mst_brand.Type AS DeviceType',
                    'mst_brand.Version AS DeviceVersion',
                    'CheckStatus AS CheckStatus',
                    'CheckDate AS CheckDate',
                    DB::raw('GROUP_CONCAT(olt_line_profile.ProfileId) AS ProfileId'),
                  ])
                  ->leftjoin('olt_line_profile', 'olt_line_profile.HostCode', '=', 'olt_host.code')
                  ->leftjoin('mst_brand', 'mst_brand.code', '=', 'olt_host.DeviceCode')
                  ->leftjoin('mst_base_transmission_station', 'mst_base_transmission_station.code', '=', 'olt_host.BtsCode')
                  ->groupBy('olt_host.Code')

                  ->find(str_replace('%20', ' ', $id));

                  return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'Hostname' => 'nullable|max:250',
            'IpAddress' => 'nullable|max:250',
            'Username' => 'nullable|max:250',
            'Password' => 'nullable|max:250',
            'Port' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'SnmpCommunity' => 'nullable|max:250',
            'Sysname' => 'nullable|max:250',
            'DeviceCode' => 'nullable|max:250',
            'Remark' => 'nullable',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new Host();
            $data->CreatedDate = currDate();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data_brand = getControllerName("Brand")::searchId("Brand",$request->DeviceCode, $request);
            if ($data_brand){
              $data->Code = generadeCode($table,"FMI-".$data_brand->Model,Null,3);
            }else{
              $data->Code = generadeCode($table,"FMI",null,5);
            }
            $data->Code = strtoupper($data->Code);
        }
        if ($request->except('Hostname')) {
            $data->Hostname = $request->Hostname;
        }
        if ($request->except('IpAddress')) {
            $data->IpAddress = $request->IpAddress;
        }
        if ($request->except('Username')) {
            $data->Username = $request->Username;
        }
        if ($request->except('Password')) {
            $data->Password = $request->Password;
        }
        if ($request->Port or $request->Port == 0) {
            $data->Port = $request->Port;
        }
        if ($request->FrameId or $request->FrameId == 0) {
            $data->FrameId = $request->FrameId;
        }
        if ($request->SlotId or $request->SlotId == 0) {
            $data->SlotId = $request->SlotId;
        }
        if ($request->PortId or $request->PortId == 0) {
            $data->PortId = $request->PortId;
        }
        if ($request->except('Sysname')) {
            $data->Sysname = $request->Sysname;
        }
        if ($request->except('SnmpCommunity')) {
            $data->SnmpCommunity = $request->SnmpCommunity;
        }
        if ($request->except('BtsCode')) {
            $data->BtsCode = $request->BtsCode;
        }
        if ($request->except('DeviceCode')) {
            $data->DeviceCode = $request->DeviceCode;
        }
        if ($request->except('Remark')) {
            $data->Remark = $request->Remark;
        }
        if ($request->except('CreatedBy')) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->except('CreatedDate')) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->except('UpdatedBy')) {
            $data->UpdatedBy = $request->UpdatedBy;
        }

        if ($request->ActiveStatus) {
            $data->ActiveStatus = 1;
        }else{
            $data->ActiveStatus = 0;
        }

        $data->UpdatedDate = currDate();
        $data->save();

        return $data;
    }

}
