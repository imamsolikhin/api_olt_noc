<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Models\GemProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GemProfileController extends Controller {

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $request) {
       $query->select('*');
       if($request->ActiveStatus){
           $query->where('ActiveStatus', '=', $request->ActiveStatus);
       }
       $query->where('HostCode', '=', isset($request->HostCode)? $request->HostCode:"None");
       return $query;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchId($table, $id, $request) {
        $data = getModelName($table)::select([
                    '*',
                  ])
                  ->find(str_replace('%20', ' ', $id));
       return $data;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null) {
        $rules = [
            'HostCode' => 'nullable|max:250',
            'ProfileId' => 'nullable|max:250',
            'GemId' => 'nullable|max:250',
            'GemMapping' => 'nullable|max:250',
            'GemVlan' => 'nullable|max:250',
            'Priority' => 'nullable|max:250',
            'Type' => 'nullable|max:250',
            'Port' => 'nullable|max:250',
            'Bundle' => 'nullable|max:250',
            'Flow' => 'nullable|max:250',
            'Transparent' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|max:20',
            'CreatedBy' => 'nullable||max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null, $table =null) {
        if (is_null($data)) {
            $data = new GemProfile();
        }

        if ($request->Code) {
            $data->Code = $request->Code;
        }else {
            $data->Code = generadeCode($table,"FMI",null,5);
        }
        if ($request->HostCode) {
            $data->HostCode = $request->HostCode;
        }
        if ($request->ProfileId) {
            $data->ProfileId = $request->ProfileId;
        }
        if ($request->GemId) {
            $data->GemId = $request->GemId;
        }
        if ($request->GemMapping) {
            $data->GemMapping = $request->GemMapping;
        }
        if ($request->GemVlan) {
            $data->GemVlan = $request->GemVlan;
        }
        if ($request->Priority) {
            $data->Priority = $request->Priority;
        }
        if ($request->Type) {
            $data->Type = $request->Type;
        }
        if ($request->Port) {
            $data->Port = $request->Port;
        }
        if ($request->Bundle) {
            $data->Bundle = $request->Bundle;
        }
        if ($request->Flow) {
            $data->Flow = $request->Flow;
        }
        if ($request->Transparent) {
            $data->Transparent = $request->Transparent;
        }
        if ($request->CreatedBy) {
            $data->CreatedBy = $request->CreatedBy;
        }
        if ($request->CreatedDate) {
            $data->CreatedDate = $request->CreatedDate;
        }
        if ($request->UpdatedBy) {
            $data->UpdatedBy = $request->UpdatedBy;
        }
        if ($request->UpdatedDate) {
            $data->UpdatedDate = $request->UpdatedDate;
        }
        if ($request->ActiveStatus) {
            $data->ActiveStatus = to_bool($request->ActiveStatus);
        }
        $data->save();

        return $data;
    }

}
