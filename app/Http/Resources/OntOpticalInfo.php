<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OntOpticalInfo extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'CustomerCode' => $this->CustomerCode,
            'HostCode' => $this->HostCode,
            'FrameId' => $this->FrameId,
            'SlotId' => $this->SlotId,
            'PortId' => $this->PortId,
            'OntId' => $this->OntId,
            'OntSn' => $this->OntSn,
            'RxOpticalPower' => $this->RxOpticalPower,
            'TxOpticalPower' => $this->TxOpticalPower,
            'Temperature' => $this->Temperature,
            'Voltage' => $this->Voltage,
            'OltRxOntOpticalPower' => $this->OltRxOntOpticalPower,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }

}
