<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientsTools extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'ClientsCode' => $this->ClientsCode,
            'Version' => $this->Version,
            'OntInfo' => $this->OntInfo,
            'WanInfo' => $this->WanInfo,
            'Opticalinfo' => $this->Opticalinfo,
            'Registerinfo' => $this->Registerinfo,
            'MacAddress' => $this->MacAddress,
            'PortState' => $this->PortState,
            'PortAttribute' => $this->PortAttribute,
            'FecCrcError' => $this->FecCrcError,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
