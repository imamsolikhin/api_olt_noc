<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerSite extends JsonResource
{

    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'LinkID' => $this->LinkID,
            'IDPelanggan' => $this->IDPelanggan,
            'CompanyName' => $this->CompanyName,
            'Province' => $this->Province,
            'City' => $this->City,
            'Address' => $this->Address,
            'ZipCode' => $this->ZipCode,
            'Website' => $this->Website,
            'Email' => $this->Email,
            'Phone' => $this->Phone,
            'Fax' => $this->Fax,
            'ProductSite' => $this->ProductSite,
            'ProductGrup' => $this->ProductGrup,
            'ProductPort' => $this->ProductPort,
            'ProductCategory' => $this->ProductCategory,
            'ProductName' => $this->ProductName,
            'ProductNotes' => $this->ProductNotes,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null
        ];
    }
}
