<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SrvPort extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'Index' => $this->Index,
            'VlanId' => $this->VlanId,
            'VlanAttr' => $this->VlanAttr,
            'Type' => $this->Type,
            'FrameId' => $this->FrameId,
            'SlotId' => $this->SlotId,
            'PortId' => $this->PortId,
            'VPI' => $this->VPI,
            'VCI' => $this->VCI,
            'FlowType' => $this->FlowType,
            'FlowPara' => $this->FlowPara,
            'Rx' => $this->Rx,
            'Tx' => $this->Tx,
            'State' => $this->State,
            'Transparent' => $this->Transparent,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
