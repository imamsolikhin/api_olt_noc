<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GemProfile extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'ProfileId' => $this->ProfileId,
            'GemId' => $this->GemId,
            'GemMapping' => $this->GemMapping,
            'GemVlan' => $this->GemVlan,
            'Priority' => $this->Priority,
            'Type' => $this->Type,
            'Port' => $this->Port,
            'Bundle' => $this->Bundle,
            'Flow' => $this->Flow,
            'Transparent' => $this->Transparent,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
