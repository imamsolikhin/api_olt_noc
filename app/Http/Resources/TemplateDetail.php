<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TemplateDetail extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HeaderCode' => $this->HeaderCode,
            'Vlan' => $this->Vlan,
            'UserVlan' => $this->UserVlan,
            'InnerVlan' => $this->InnerVlan,
            'TagTransform' => $this->TagTransform,
            'TrafficTable' => $this->TrafficTable,
            'GemPort' => $this->GemPort,
            'Remark' => $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,

        ];
    }

}
