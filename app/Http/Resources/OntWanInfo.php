<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OntWanInfo extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'CustomerCode' => $this->CustomerCode,
            'HostCode' => $this->HostCode,
            'FrameId' => $this->FrameId,
            'SlotId' => $this->SlotId,
            'PortId' => $this->PortId,
            'OntId' => $this->OntId,
            'OntSn' => $this->OntSn,
            'MacAddress' => $this->MacAddress,
            'ServiceType' => $this->ServiceType,
            'ConnectionType' => $this->ConnectionType,
            'ConnectionStatus' => $this->ConnectionStatus,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }

}
