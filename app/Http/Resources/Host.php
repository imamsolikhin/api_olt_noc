<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Host extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Hostname' => $this->Hostname,
            'IpAddress' => $this->IpAddress,
            'Username' => $this->Username,
            'Password' => $this->Password,
            'Remark' => $this->Remark,
            'CurrentConfig' => $this->CurrentConfig,
            'Port' => $this->Port,
            'FrameId' => $this->FrameId,
            'SlotId' => $this->SlotId,
            'PortId' => $this->PortId,
            'SnmpCommunity' => $this->SnmpCommunity,
            'Sysname' => $this->Sysname,
            'BtsCode' => $this->BtsCode,
            'BtsName' => $this->BtsName,
            'BtsAddress' => $this->BtsAddress,
            'DeviceCode' => $this->DeviceCode,
            'DeviceName' => $this->DeviceName,
            'DeviceModel' => $this->DeviceModel,
            'DeviceType' => $this->DeviceType,
            'DeviceVersion' => $this->DeviceVersion,
            'CheckStatus' => $this->CheckStatus,
            'ProfileId' => $this->ProfileId,
            'CheckDate' => $this->CheckDate != null ? date('Y-m-d H:i:s', strtotime($this->CheckDate)) : null,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
