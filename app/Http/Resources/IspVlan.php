<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IspVlan extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'IspCode' => $this->IspCode,
            'HostCode' => $this->HostCode,
            'HostName' => $this->HostName,
            'F' => $this->F,
            'S' => $this->S,
            'P' => $this->P,
            'Note' => $this->Note,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
