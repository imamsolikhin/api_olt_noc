<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Ont extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'Sn' => $this->Sn,
            'OntId' => $this->OntId,
            'Version' => $this->Version,
            'SoftwareVersion' => $this->SoftwareVersion,
            'EquipmentId' => $this->EquipmentId,
            'FrameId' => $this->FrameId,
            'SlotId' => $this->SlotId,
            'PortId' => $this->PortId,
            'BoardFrameId' => $this->BoardFrameId,
            'BoardSlotId' => $this->BoardSlotId,
            'BoardPortId' => $this->BoardPortId,
            'RxPower' => $this->PortId,
            'TxPower' => $this->PortId,
            'Remark' => $this->Remark,
            'Hostname' =>  $this->Hostname,
            'IpAddress' =>  $this->IpAddress,
            'Port' =>  $this->Port,
            'Sysname' =>  $this->Sysname,
            'SnmpCommunity' =>  $this->SnmpCommunity,
            'Username' =>  $this->Username,
            'Password' =>  $this->Password,
            'HistoryOntSn' => $this->HistoryOntSn,
            'HistoryRemark' => $this->HistoryRemark,
            'HistoryStatus' => $this->HistoryStatus,
            'HistoryDate' => $this->HistoryDate,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
