<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Customer extends JsonResource {

    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'LinkID' => $this->LinkID,
            'MemberID' => $this->MemberID,
            'JobTitle' => $this->JobTitle,
            'ContactPerson' => $this->ContactPerson,
            'Email' => $this->Email,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'PhoneNumber' => $this->PhoneNumber,
            'NPWP' => $this->NPWP,
            'Fax' => $this->Fax,
            'Address' => $this->Address,
            'CityCode' => $this->CityCode,
            'City' => $this->City,
            'Province' => $this->Province,
            'ZipCode' => $this->ZipCode,
            'Website' => $this->Website,
            'IspCode' => $this->IspCode,
            'IspName' => $this->IspName,
            'IspContactPerson' => $this->IspContactPerson,
            'IspAddress' => $this->IspAddress,
            'IspCity' => $this->IspCity,
            'IspPhone1' => $this->IspPhone1,
            'IspPhone2' => $this->IspPhone2,
            'IspFax' => $this->IspFax,
            'IspEmail' => $this->IspEmail,
            'IDCard' => $this->IDCard,
            'IDCardNumber' => $this->IDCardNumber,
            'IDCardExpired' => $this->IDCardExpired,
            'RegistrationDate' => $this->RegistrationDate,
            'ProductCode' => $this->ProductCode,
            'ClientStatus' => $this->ClientStatus,
            'ProductSite' => $this->ProductSite,
            'ProductGrup' => $this->ProductGrup,
            'ProductPort' => $this->ProductPort,
            'ProductCategory' => $this->ProductCategory,
            'BaseTransmissionStationCode' => $this->BaseTransmissionStationCode,
            'ProductName' => $this->ProductName,
            'ProductNotes' => $this->ProductNotes,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}
