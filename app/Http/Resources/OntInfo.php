<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OntInfo extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'CustomerCode' => $this->CustomerCode,
            'HostCode' => $this->HostCode,
            'FrameId' => $this->FrameId,
            'SlotID' => $this->SlotID,
            'PortId' => $this->PortId,
            'OntId' => $this->OntId,
            'OntSn' => $this->OntSn,
            'Flag' => $this->Flag,
            'Run' => $this->Run,
            'Config' => $this->Config,
            'Match' => $this->Match,
            'OntDistance' => $this->OntDistance,
            'Temperature' => $this->Temperature,
            'Description' => $this->Description,
            'LastDownCause' => $this->LastDownCause,
            'LastUpTime' => $this->LastUpTime,
            'LastDownTime' => $this->LastDownTime,
            'LastDyingGaspTime' => $this->LastDyingGaspTime,
            'OntOnlineDuration' => $this->OntOnlineDuration,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }

}
