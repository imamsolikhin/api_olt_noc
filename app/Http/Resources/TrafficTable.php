<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrafficTable extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'Index' => $this->Index,
            'Name' => $this->Name,
            'Cir' => $this->Cir,
            'Cbs' => $this->Cbs,
            'Pir' => $this->Pir,
            'Pbs' => $this->Pbs,
            'Priority' => $this->Priority,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
