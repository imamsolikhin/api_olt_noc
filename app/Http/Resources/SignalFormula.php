<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SignalFormula extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Formula' => $this->Formula,
            'TxPower' => $this->TxPower,
            'Distance' => $this->Distance,
            'LossDb' => $this->LossDb,
            'LossFixTop' => $this->LossFixTop,
            'LossFixBot' => $this->LossFixBot,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }

}
