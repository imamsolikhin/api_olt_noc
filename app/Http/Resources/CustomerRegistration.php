<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerRegistration extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'CompanyCode' => $this->CompanyCode,
            'CompanyName' => $this->CompanyName,
            'CompanyInisial'=> $this->CompanyInisial,
            'CompanyGroup'=> $this->CompanyGroup,
            'LineofBusiness'=> $this->LineofBusiness,
            'CustomerCode'=> $this->CustomerCode,
            'CustomerCompanyCode'=> $this->CustomerCompanyCode,
            'CustomerCompanyName'=> $this->CustomerCompanyName,
            'CustomerProvinceBirth'=> $this->CustomerProvinceBirth,
            'CustomerCityBirth'=> $this->CustomerCityBirth,
            'CustomerBirthDate'=> $this->CustomerBirthDate,
            'CustomerJobTitle'=> $this->CustomerJobTitle,
            'CustomerPhone'=> $this->CustomerPhone,
            'CustomerFax'=> $this->CustomerFax,
            'CustomerEmail'=> $this->CustomerEmail,
            'CustomerPhoneNumber'=> $this->CustomerPhoneNumber,
            'CustomerNPWP'=> $this->CustomerNPWP,
            'CustomerAddress'=> $this->CustomerAddress,
            'CustomerProvinceCode'=> $this->CustomerProvinceCode,
            'CustomerProvinceName'=> $this->CustomerProvinceName,
            'CustomerCityCode'=> $this->CustomerCityCode,
            'CustomerCityName'=> $this->CustomerCityName,
            'CustomerZipCode'=> $this->CustomerZipCode,
            'CustomerWebsite'=> $this->CustomerWebsite,
            'CustomerIDCard'=> $this->CustomerIDCard,
            'CustomerIDCardNumber'=> $this->CustomerIDCardNumber,
            'CustomerIDCardExpired'=> $this->CustomerIDCardExpired,
            'CustomerProductSite'=> $this->CustomerProductSite,
            'CustomerRegistrationDate'=> $this->CustomerRegistrationDate,
            'CustomerProductCode'=> $this->CustomerProductCode,
            'CustomerStatusClient'=> $this->CustomerStatusClient,
            'CustomerSalesCode'=> $this->CustomerSalesCode,
            'CustomerSalesName'=> $this->CustomerSalesName,
            'TechnicalPicCode'=> $this->TechnicalPicCode,
            'TechnicalPicCompanyCode'=> $this->TechnicalPicCompanyCode,
            'TechnicalPicCompanyName'=> $this->TechnicalPicCompanyName,
            'TechnicalPicName'=> $this->TechnicalPicName,
            'TechnicalPicDepartment'=> $this->TechnicalPicDepartment,
            'TechnicalPicJobTitle'=> $this->TechnicalPicJobTitle,
            'TechnicalPicPhoneNumber'=> $this->TechnicalPicPhoneNumber,
            'TechnicalPicFax'=> $this->TechnicalPicFax,
            'TechnicalPicEmail'=> $this->TechnicalPicEmail,
            'TechnicalPicSalesInfo'=> $this->TechnicalPicSalesInfo,
            'TechnicalPicNIP'=> $this->TechnicalPicNIP,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
