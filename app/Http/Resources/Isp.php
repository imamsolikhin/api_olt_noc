<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Isp extends JsonResource
{

    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'ContactPerson' => $this->ContactPerson,
            'Address' => $this->Address,
            'CityCode' => $this->CityCode,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'Fax' => $this->Fax,
            'Email' => $this->Email,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null
        ];
    }
}
