<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientsVlanService extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'CustomerCode' => $this->CustomerCode,
            'ServicePortId' => $this->ServicePortId,
            'Vlan' => $this->Vlan,
            'GemPort' => $this->GemPort,
            'UserVlan' => $this->UserVlan,
            'InnerVlan' => $this->InnerVlan,
            'TagTransform' => $this->TagTransform,
            'TrafficTable' => $this->TrafficTable,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }

}
