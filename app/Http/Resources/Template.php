<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Template extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'VlanDownLink' => $this->VlanDownLink,
            'OntLineProfileId' => $this->OntLineProfileId,
            'OntSrvProfileId' => $this->OntSrvProfileId,
            'NativeVlanEth1' => $this->NativeVlanEth1,
            'NativeVlanEth2' => $this->NativeVlanEth2,
            'NativeVlanEth3' => $this->NativeVlanEth3,
            'NativeVlanEth4' => $this->NativeVlanEth4,
            'VlanAttribut' => $this->VlanAttribut,
            'Remark' => $this->Remark,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,

        ];
    }

}
