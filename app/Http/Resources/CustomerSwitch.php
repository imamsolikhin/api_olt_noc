<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerSwitch extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'CompanyName' => $this->CompanyName,
            'LinkID' => $this->LinkID,
            'MemberID' => $this->MemberID,
            'JobTitle' => $this->JobTitle,
            'ContactPerson' => $this->ContactPerson,
            'Email' => $this->Email,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'PhoneNumber' => $this->PhoneNumber,
            'NPWP' => $this->NPWP,
            'Fax' => $this->Fax,
            'Address' => $this->Address,
            'CityCode' => $this->CityCode,
            'City' => $this->City,
            'ZipCode' => $this->ZipCode,
            'Website' => $this->Website,
            'Province' => $this->Province,
            'IDCard' => $this->IDCard,
            'IDCardNumber' => $this->IDCardNumber,
            'IDCardExpired' => $this->IDCardExpired,
            'RegistrationDate' => $this->RegistrationDate,
            'ProductCode' => $this->ProductCode,
            'ClientStatus' => $this->ClientStatus,
            'ProductSite' => $this->ProductSite,
            'ProductGrup' => $this->ProductGrup,
            'ProductPort' => $this->ProductPort,
            'ProductCategory' => $this->ProductCategory,
            'ProductName' => $this->ProductName,
            'ProductNotes' => $this->ProductNotes,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
