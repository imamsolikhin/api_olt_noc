<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InterfaceGpon extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'FrameId' => $this->FrameId,
            'SlotID' => $this->SlotID,
            'PortId' => $this->PortId,
            'OntId' => $this->OntId,
            'OntSn' => $this->OntSn,
            'Description' => $this->Description,
            'Flag' => $this->Flag,
            'Run' => $this->Run,
            'Config' => $this->Config,
            'Match' => $this->Match,
            'Protect' => $this->Protect,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
