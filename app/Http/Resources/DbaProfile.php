<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DbaProfile extends JsonResource {

    public function toArray($request) {
        return [
            'Code' => $this->Code,
            'HostCode' => $this->HostCode,
            'ProfileId' => $this->ProfileId,
            'Type' => $this->Type,
            'Bandwidth' => $this->Bandwidth,
            'Fix' => $this->Fix,
            'Assure' => $this->Assure,
            'Max' => $this->Max,
            'Bind' => $this->Bind,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
        ];
    }

}
