<?php

namespace App\Services;

use phpseclib\Crypt\RSA;

class Signature
{
    private $privateKey;
    private $plainText;

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Create a new class instance.
     *
     * @return void
     */
    public function __construct($privateKey, $plainText)
    {
        $this->privateKey = $privateKey;
        $this->plainText = $plainText;
    }

    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Make a Signature.
     *
     * @return string
     */
    public function create()
    {
        $rsa = new RSA;
        $rsa->setHash('sha256');
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);

        $rsa->loadKey($this->privateKey);

        return base64_encode($rsa->sign($this->plainText));
    }
}
