<?php

/*
* Inkombizz | inkombizz@gmail.com | inkombizz.com
* Set The Secrets
*/
return [
    /*
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Set Public Key (string)
     */
    'public_key' => env('PUBLIC_KEY_SECRET'),
];