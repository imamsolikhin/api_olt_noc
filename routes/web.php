<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

/**
 * Inkombizz | inkombizz@gmail.com | inkombizz.com
 * Route for Documentation.
 */
$router->get('/', function () {
    return redirect('https://documenter.getpostman.com/view/5385605/T17Na4jG');
});

/**
 * Inkombizz | inkombizz@gmail.com | inkombizz.com
 * Route Group for API.
 */
$router->group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'api'], function () use ($router) {
    /**
     * Inkombizz | inkombizz@gmail.com | inkombizz.com
     * Route Group for Version 1.
     */
    $router->group(['prefix' => 'v1', 'namespace' => 'v1', 'middleware' => 'signature'], function () use ($router) {
        /**
         * Inkombizz | inkombizz@gmail.com | inkombizz.com
         * Route for Customer.
         */
        $router->group(['prefix' => '{table}'], function () use ($router) {
            $router->get('/dashboard', [
                'as' => 'olt.dashboard', 'uses' => 'OltController@dashboard'
            ]);
            $router->get('/', [
                'as' => 'olt.index', 'uses' => 'OltController@index'
            ]);
            $router->post('/', [
                'as' => 'olt.store', 'uses' => 'OltController@store'
            ]);
            $router->get('{id}', [
                'as' => 'olt.show', 'uses' => 'OltController@show'
            ]);
            $router->put('{id}', [
                'as' => 'olt.update', 'uses' => 'OltController@update'
            ]);
            $router->delete('{id}', [
                'as' => 'olt.destroy', 'uses' => 'OltController@destroy'
            ]);
        });
    });
});
